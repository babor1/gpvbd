﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ComplaintsViewUpdate.aspx.cs" Inherits="GpvbdProj.ComplaintsViewUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Complaints </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Complaints View Update</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body" style="padding: .10rem 1rem;">
                    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvComplaintsUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div style="margin: 10px; overflow-x: auto">
                                <asp:GridView ID="gvComplain" runat="server" DataKeyNames="id" OnRowCommand="gvComplain_RowCommand" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
                                        <asp:BoundField DataField="Village" HeaderText="Village" />
                                        <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                        <asp:BoundField DataField="IsSolved" HeaderText="Is Solved ?" />
                                        <asp:BoundField DataField="SolvedDate" HeaderText="Solved Date" />
                                        <asp:TemplateField HeaderText="Image">
                                            <ItemTemplate>
                                                <asp:Image ID="Img" runat="server" Width="70px" Height="95px" ImageUrl='<%# Eval("path") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:Button ID="btnSolved" runat="server" CssClass="mdi mdi-check-circle" Text="Solved" CommandName="Solved" CommandArgument='<%# Eval("id") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle CssClass="GridViewEditRow" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
