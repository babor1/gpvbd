﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GpvbdProj.Model;
using System.Data;

namespace GpvbdProj.Reports
{
    public class ComplaintsDSClass : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        public DataSet getFever(bool solved)
        {
            string lpath = "";
            PnchtId = Int32.Parse(Session["pc"].ToString());
            var ld = db.PublicComplains.Where(m => m.isSolved == solved && m.PanchayetId == PnchtId);

            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FatherName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Village", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("MobileNo", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("IsSolved", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SolvedDate", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Path", Type.GetType("System.String")));

            foreach (PublicComplain l in ld)
            {
                DataRow dr = dt.NewRow();

                dr[0] = l.Name;
                dr[1] = l.FatherName;
                dr[2] = l.Village;
                dr[3] = l.Mobile;
                if (l.isSolved == true)
                    dr[4] = "Yes";
                else
                    dr[4] = "No";
                dr[5] = Convert.ToDateTime(l.SolvedDate).ToString("dd-MM-yyy");
                if (l.Path != null)
                {
                    lpath = l.Path.Remove(0, 13);
                    dr[6] = lpath;
                }
                else
                    dr[6] = l.Path;


                dt.Rows.Add(dr);
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);
            return dd;
        }
    }
}