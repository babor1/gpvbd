﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GpvbdProj.Model;

namespace GpvbdProj.Reports
{
    public class FeverDSClass : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        public DataSet getFever(bool solved)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            var ld = db.FeverMasters.Where(m => m.isSolved == solved && m.PanchayetId==PnchtId);

            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("SansadId", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SansadName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("HouseNo", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FeverPatientName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FeverPatientFatherName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FatherName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("DayOfFever", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Area", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("MobileNo", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("EntryDate", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("VRP_SHG", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ASHA_ANM", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SUB_CENTER", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("IsSolved", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SolvedDate", Type.GetType("System.String")));


            foreach (FeverMaster l in ld)
            {
                DataRow dr = dt.NewRow();

                dr[0] = l.fmId;
                dr[1] = l.SansadName;
                dr[2] = l.HouseNo;
                dr[3] = l.FeverPatientName;
                dr[4] = l.FeverFatherPatientName;
                dr[5] = l.Name;
                dr[6] = l.FatherName;
                dr[7] = l.DayofFever;
                dr[8] = l.Area;
                dr[9] = l.MobileNo;
                dr[10] = Convert.ToDateTime(l.EntryDate).ToString("dd-MM-yyy");
                dr[11] = l.SansadMaster.VRP_SHG;
                dr[12] = l.SansadMaster.ASHA_ANM;
                dr[13] = l.SansadMaster.Sub_Center;
                if(l.isSolved==true)
                    dr[14] = "Yes";
                else
                    dr[14] = "No";
                dr[15] = Convert.ToDateTime(l.SolvedDate).ToString("dd-MM-yyy");

                dt.Rows.Add(dr);
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);
            return dd;
        }
    }
}