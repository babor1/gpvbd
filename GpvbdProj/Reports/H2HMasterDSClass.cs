﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GpvbdProj.Model;
using System.Data;
using System.Text.RegularExpressions;

namespace GpvbdProj.Reports
{
    public class H2HMasterDSClass : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        public DataSet getH2HMaster(string Sansad)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            if(Sansad!="Select")
            {
                var ld = db.H2HMaster.Where(m => m.PanchayetId == PnchtId && m.SansadName == Sansad).ToList();

                var result = ld.OrderBy(k => PadNumbers(k.HouseNo));

                DataTable dt = new DataTable();

                dt.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("FatherName", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("SansadName", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("FamilyMember", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("MobileNo", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("HouseNo", Type.GetType("System.String")));

                foreach (H2HMaster l in result)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = l.Name;
                    dr[1] = l.FatherName;
                    dr[2] = l.SansadName;
                    dr[3] = l.FamilyMember;
                    dr[4] = l.MobileNo;
                    dr[5] = l.HouseNo;

                    dt.Rows.Add(dr);
                }

                DataSet dd = new DataSet();
                dd.Tables.Add(dt);
                return dd;
            }
            else
            {
                var ld = db.H2HMaster.Where(m => m.PanchayetId == PnchtId).ToList();
                var result = ld.OrderBy(k => PadNumbers(k.HouseNo));

                DataTable dt = new DataTable();

                dt.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("FatherName", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("SansadName", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("FamilyMember", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("MobileNo", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("HouseNo", Type.GetType("System.String")));

                foreach (H2HMaster l in result)
                {
                    DataRow dr = dt.NewRow();

                    dr[0] = l.Name;
                    dr[1] = l.FatherName;
                    dr[2] = l.SansadName;
                    dr[3] = l.FamilyMember;
                    dr[4] = l.MobileNo;
                    dr[5] = l.HouseNo;

                    dt.Rows.Add(dr);
                }

                DataSet dd = new DataSet();
                dd.Tables.Add(dt);
                return dd;
            }
            
        }
        public static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }
    }
}