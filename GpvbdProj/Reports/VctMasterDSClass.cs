﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GpvbdProj.Model;
using System.Data;

namespace GpvbdProj.Reports
{
    public class VctMasterDSClass : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        public DataSet getVctMaster(bool solved)
        {
            string lpath = "";
            PnchtId = Int32.Parse(Session["pc"].ToString());
            var ld = db.VCTMasters.Where(m => m.isSolved == solved && m.PanchayetId == PnchtId);

            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("FatherName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SansadName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("LocationType", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("MobileNo", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("IsSolved", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SolvedDate", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Path", Type.GetType("System.String"))); 
                dt.Columns.Add(new DataColumn("HouseNo", Type.GetType("System.String"))); 

            foreach (VCTMaster l in ld)
            {
                DataRow dr = dt.NewRow();

                dr[0] = l.Name;
                dr[1] = l.FatherName;
                dr[2] = l.SansadName;
                dr[3] = l.LocationType;
                dr[4] = l.MobileNo;
                if (l.isSolved == true)
                    dr[5] = "Yes";
                else
                    dr[5] = "No";
                dr[6] = Convert.ToDateTime(l.SolvedDate).ToString("dd-MM-yyy");
                if (l.path != null)
                {
                    lpath = l.path.Remove(0, 14);
                    dr[7] = lpath;
                }
                else
                    dr[7] = l.path;
                dr[8] = l.HouseNo;

                dt.Rows.Add(dr);
            }

            DataSet dd = new DataSet();
            dd.Tables.Add(dt);
            return dd;
        }
    }
}