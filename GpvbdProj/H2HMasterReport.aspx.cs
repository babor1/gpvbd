﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using GpvbdProj.Reports;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace GpvbdProj
{
    public partial class H2HMasterReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                getH2HMasterReport();
            }
        }
        public void getH2HMasterReport()
        {
            try
            {
                this.ReportViewer1.LocalReport.EnableExternalImages = true;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/H2HMasterRpt.rdlc");

                H2HMasterDSClass tr = new H2HMasterDSClass();
                DataSet ds = new DataSet();

                string sn = Request.QueryString["sn"].ToString();
                ds = tr.getH2HMaster(sn);

                // set above path to report parameter
                var parameter = new ReportParameter[2];
                parameter[0] = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));
                string pc = "";
                if (Int32.Parse(Session["pc"].ToString()) == 1)
                    pc = "RADHARGHAT-1";
                else
                    pc = "SAHAJADPUR";

                parameter[1] = new ReportParameter("Panchayet", pc);
                ReportViewer1.LocalReport.SetParameters(parameter);

                ReportDataSource datasource = new ReportDataSource("H2HMasterDS", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }
    }
}