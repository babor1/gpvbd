﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GpvbdProj.Model;
using EntityFramework.Extensions;
using System.Text.RegularExpressions;

namespace GpvbdProj
{
    public partial class h2hMaster : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getSansadDDL(PnchtId);
                getH2hMasterGrid(PnchtId,"Select");

            }
        }

        private void getSansadDDL(int PnchtId)
        {
            var c = db.SansadMasters.Where(l => l.PanchayetId == PnchtId).Select(m => new { id = m.GSId, Name = m.GramSansadName }).ToList();

            ddlSansadNo.DataSource = c;
            ddlSansadNo.DataValueField = "id";
            ddlSansadNo.DataTextField = "name";
            ddlSansadNo.DataBind();
            ddlSansadNo.Items.Insert(0, new ListItem("Select", "0"));
            ddlSansadNo.SelectedIndex = -1;

            ddlViewSansad.DataSource = c;
            ddlViewSansad.DataValueField = "id";
            ddlViewSansad.DataTextField = "name";
            ddlViewSansad.DataBind();
            ddlViewSansad.Items.Insert(0, new ListItem("Select", "0"));
            ddlViewSansad.SelectedIndex = -1;

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ddlSansadNo.SelectedItem.Text != "Select")
            {
                H2HMaster h = new H2HMaster();
                int gsId = Int32.Parse(ddlSansadNo.SelectedItem.Value);
                h.GSId = gsId;
                h.SansadName = ddlSansadNo.SelectedItem.Text;
                h.HouseNo = txtHouseNo.Text;
                h.Name = txtName.Text.ToUpper();
                h.FatherName = txtFatherName.Text.ToUpper();
                h.FamilyMember = Int32.Parse(txtFamilyMember.Text);
                h.MobileNo = txtMobileNo.Text;
                PnchtId = Int32.Parse(Session["pc"].ToString());
                h.PanchayetId = PnchtId;

                db.H2HMaster.Add(h);
                db.SaveChanges();
                PnchtId = Int32.Parse(Session["pc"].ToString());
                this.getH2hMasterGrid(PnchtId,"Select");
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Select Sansad Name')", true);

            reset();

        }

        private void getH2hMasterGrid(int PnchtId,string sansad)
        {
            
            if (sansad=="Select")
            {
                var s = db.H2HMaster.Where(l => l.PanchayetId == PnchtId)
                    .Select(m => new { id = m.h2hMid, HouseNo = m.HouseNo, MobileNo = m.MobileNo, SansadName = m.SansadName, FatherName = m.FatherName, Name = m.Name, FamilyMember = m.FamilyMember })
                    //.OrderBy(k => PadNumbers(k.HouseNo))
                    .ToList();

                var result = s.OrderBy(k => PadNumbers(k.HouseNo));

                gvH2hMaster.DataSource = result;
                gvH2hMaster.DataBind();
            }
            else
            {
                var s = db.H2HMaster.Where(l => l.PanchayetId == PnchtId && l.SansadName==sansad)
                    .Select(m => new { id = m.h2hMid, HouseNo = m.HouseNo, MobileNo = m.MobileNo, SansadName = m.SansadName, FatherName = m.FatherName, Name = m.Name, FamilyMember = m.FamilyMember })
                    //.OrderBy(k => PadNumbers(k.HouseNo))
                    .ToList();
                var result = s.OrderBy(k => PadNumbers(k.HouseNo));

                gvH2hMaster.DataSource = result;
                gvH2hMaster.DataBind();
            }
            
        }

        public static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }
        private void reset()
        {
            //ddlSansadNo.SelectedIndex=-1;
            //txtHouseNo.Text = "";
            txtName.Text = "";
            txtFatherName.Text = "";
            txtFamilyMember.Text = "";
            txtMobileNo.Text = "";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }

        protected void ddlSansadNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtHouseNo.Text = ddlSansadNo.SelectedItem.Text.ToString() + "_";
        }

        protected void gvH2hMaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvH2hMaster.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvH2hMaster_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvH2hMaster.EditIndex = e.NewEditIndex;
            //gvH2hMaster.Width = Unit.Percentage(100);
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hMasterGrid(PnchtId,ddlViewSansad.SelectedItem.Text);
            GridViewRow row = gvH2hMaster.Rows[e.NewEditIndex];
            (row.Cells[2].Controls[0] as TextBox).Width = Unit.Pixel(100);
            (row.Cells[3].Controls[0] as TextBox).Width = Unit.Pixel(100);
            //(row.Cells[4].Controls[0] as TextBox).Width = Unit.Pixel(20);
            //(row.Cells[5].Controls[0] as TextBox).Width = Unit.Pixel(20);
            (row.Cells[6].Controls[0] as TextBox).Width = Unit.Pixel(100);
            (row.Cells[7].Controls[0] as TextBox).Width = Unit.Pixel(100);
        }

        protected void gvH2hMaster_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvH2hMaster.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hMasterGrid(PnchtId,ddlViewSansad.SelectedItem.Text);
        }

        protected void gvH2hMaster_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvH2hMaster.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvH2hMaster.DataKeys[e.RowIndex].Values[0]);
            string SansadName = (row.Cells[2].Controls[0] as TextBox).Text;
            string HouseNo = (row.Cells[3].Controls[0] as TextBox).Text;
            string Name = (row.Cells[4].Controls[0] as TextBox).Text;
            string FatherName = (row.Cells[5].Controls[0] as TextBox).Text;
            string FamilyMember = (row.Cells[6].Controls[0] as TextBox).Text;
            string MobileNo = (row.Cells[7].Controls[0] as TextBox).Text;

            H2HMaster m = db.H2HMaster.Single(s => s.h2hMid == id);

            m.SansadName = SansadName;
            m.HouseNo = HouseNo;
            m.Name = Name.ToUpper();
            m.FatherName = FatherName.ToUpper();
            m.FamilyMember = Int32.Parse(FamilyMember);
            m.MobileNo = MobileNo;

            db.SaveChanges();


            gvH2hMaster.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hMasterGrid(PnchtId, ddlViewSansad.SelectedItem.Text);
        }

        protected void gvH2hMaster_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvH2hMaster.DataKeys[e.RowIndex].Values[0]);
            db.H2HMaster.Where(s => s.h2hMid == id).Delete();
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hMasterGrid(PnchtId,ddlViewSansad.SelectedItem.Text);
        }

        protected void btnGetReoprt_Click(object sender, EventArgs e)
        {
            Response.Redirect("/H2HMasterReport.aspx?sn=" + ddlViewSansad.SelectedItem.Text);
        }

        protected void ddlViewSansad_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getH2hMasterGrid(PnchtId, ddlViewSansad.SelectedItem.Text);
        }
    }
}