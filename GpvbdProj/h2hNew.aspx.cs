﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class h2hNew : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getH2hGrid(PnchtId);
                try
                {
                    db.FeverMasters.Where(s => s.H2Hid == null).Delete();

                }
                catch (Exception)
                {

                }

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //           if (ddlSansadName.SelectedItem.Text != "Select" && ddlMonth.SelectedItem.Text != "Select" && ddlWeek.SelectedItem.Text != "Select" && ddlDay.SelectedItem.Text != "Select")
            //           {
            //               if (btnGetFevCount())
            //               {
            //                   H2H h = new H2H();
            //                   h.month = ddlMonth.SelectedItem.Text;
            //                   h.week = Int32.Parse(ddlWeek.SelectedValue);
            //                   h.day = Int32.Parse(ddlDay.SelectedValue);
            //                   int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            //                   h.GSId = gsId;
            //                   h.GramSansadName = ddlSansadName.SelectedItem.Text;
            //                   h.BlockId = Int32.Parse(txtBlockId.Text);
            //                   h.DistrictId = Int32.Parse(txtDistrictId.Text);
            //                   h.GramPanchayatId = Int32.Parse(txtGramPanchayatId.Text);
            //                   h.GramSansadId = Int32.Parse(txtGramSansadId.Text);
            //                   h.PopulationOfAreaVisited = Int32.Parse(txtPopulationOfAreaVisited.Text);
            //                   h.NumberOfHouseholdsVisitedFamilies = Int32.Parse(txtNumberOfHouseholdsVisitedFamilies.Text);
            //                   h.NumberOfPersonHavingFeverDatewise = Int32.Parse(txtNumberOfPersonHavingFeverDatewise.Text);
            //                   h.NumberOfContainerChecked = Int32.Parse(txtNumberOfContainerChecked.Text);
            //                   h.NumberOfContainerHavingMosquitoLarva = Int32.Parse(txtNumberOfContainerHavingMosquitoLarva.Text);
            //                   h.NumberOfDrainAndSoakPitChecked = Int32.Parse(txtNumberOfDrainAndSoakPitChecked.Text);
            //                   h.NumberOfDrainAndSoakPitHavingMosquitoLarva = Int32.Parse(txtNumberOfDrainAndSoakPitHavingMosquitoLarva.Text);
            //                   h.NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople = Int32.Parse(txtNumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople.Text);
            //                   h.NumberOfLeafletDistribution = Int32.Parse(txtNumberOfLeafletDistribution.Text);

            //                   PnchtId = Int32.Parse(Session["pc"].ToString());
            //                   h.PanchayetId = PnchtId;

            //                   db.H2H.Add(h);
            //                   db.SaveChanges();

            //                   SubmitedFeveCaseWithThisEntry(h.h2hId);
            //                   PnchtId = Int32.Parse(Session["pc"].ToString());
            //                   this.getH2hGrid(ddlsearch.Items[1].Value, 0, 0, PnchtId); ;
            //               }
            //               else
            //               {
            //                   ClientScript.RegisterStartupScript(typeof(Page), "alertMessage",
            //"<script type='text/javascript'>alert('Please Submit Proper Number of Fever Case!!!');window.location.replace('h2h.aspx');</script>");
            //               }

            //           }
            //           else
            //               ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Select Sansad Name')", true);

            //           reset();
        }
        private void SubmitedFeveCaseWithThisEntry(int id)
        {
            var feb = db.FeverMasters.Where(s => s.H2Hid == null);
            foreach (FeverMaster f in feb)
            {
                f.H2Hid = id;
            }
            db.SaveChanges();
        }
        private void getH2hGrid(int PnchtId)
        {

            var s = db.H2H.Where(l => l.PanchayetId == PnchtId).Select(m => new
            {
                id = m.h2hId,
                SansadName = m.GramSansadName,
                SansadId = m.GramSansadId,
                PopulationOfAreaVisited = m.PopulationOfAreaVisited,
                NumberOfHouseholdsVisitedFamilies = m.NumberOfHouseholdsVisitedFamilies,
                NumberOfPersonHavingFeverDatewise = m.NumberOfPersonHavingFeverDatewise,
                NumberOfContainerChecked = m.NumberOfContainerChecked,
                NumberOfContainerHavingMosquitoLarva = m.NumberOfContainerHavingMosquitoLarva,
                NumberOfDrainAndSoakPitChecked = m.NumberOfDrainAndSoakPitChecked,
                NumberOfDrainAndSoakPitHavingMosquitoLarva = m.NumberOfDrainAndSoakPitHavingMosquitoLarva,
                NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople = m.NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople,
                NumberOfLeafletDistribution = m.NumberOfLeafletDistribution
            }).ToList();
            gvH2h.DataSource = s;
            gvH2h.DataBind();




        }
        private void reset()
        {
            //ddlSansadName.SelectedIndex = -1;
            //ddlDay.SelectedIndex = -1;
            //ddlWeek.SelectedIndex = -1;
            //txtBlockId.Text = "";
            //txtDistrictId.Text = "";
            //txtGramPanchayatId.Text = "";
            //txtGramSansadId.Text = "";
            //txtPopulationOfAreaVisited.Text = "";
            //txtNumberOfHouseholdsVisitedFamilies.Text = "";
            txtNumberOfPersonHavingFeverDatewise.Text = "";
            txtNumberOfContainerCheckedInOut.Text = "";
            //txtNumberOfContainerHavingMosquitoLarva.Text = "";
            txtNumberOfDrainAndSoakPitChecked.Text = "";
            txtNumberOfDrainAndSoakPitHavingMosquitoLarva.Text = "";

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }



        protected void gvH2h_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvH2h.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvH2h_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvH2h.EditIndex = e.NewEditIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hGrid(PnchtId); 
        }

        protected void gvH2h_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvH2h.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hGrid(PnchtId);
        }


        protected void gvH2h_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvH2h.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvH2h.DataKeys[e.RowIndex].Values[0]);
            string SansadName = (row.Cells[2].Controls[0] as TextBox).Text;
            string SansadId = (row.Cells[3].Controls[0] as TextBox).Text;
            string PopulationOfAreaVisited = (row.Cells[4].Controls[0] as TextBox).Text;
            string NumberOfHouseholdsVisitedFamilies = (row.Cells[5].Controls[0] as TextBox).Text;
            string NumberOfPersonHavingFeverDatewise = (row.Cells[6].Controls[0] as TextBox).Text;
            string NumberOfContainerChecked = (row.Cells[7].Controls[0] as TextBox).Text;
            string NumberOfContainerHavingMosquitoLarva = (row.Cells[8].Controls[0] as TextBox).Text;
            string NumberOfDrainAndSoakPitChecked = (row.Cells[9].Controls[0] as TextBox).Text;
            string NumberOfDrainAndSoakPitHavingMosquitoLarva = (row.Cells[10].Controls[0] as TextBox).Text;
            string NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople = (row.Cells[11].Controls[0] as TextBox).Text;
            string NumberOfLeafletDistribution = (row.Cells[12].Controls[0] as TextBox).Text;



            H2H m = db.H2H.Single(s => s.h2hId == id);

            m.GramSansadName = SansadName;
            m.GramSansadId = Int32.Parse(SansadId);
            m.PopulationOfAreaVisited = Int32.Parse(PopulationOfAreaVisited);
            m.NumberOfHouseholdsVisitedFamilies = Int32.Parse(NumberOfHouseholdsVisitedFamilies);
            m.NumberOfPersonHavingFeverDatewise = Int32.Parse(NumberOfPersonHavingFeverDatewise);
            m.NumberOfContainerChecked = Int32.Parse(NumberOfContainerChecked);
            m.NumberOfContainerHavingMosquitoLarva = Int32.Parse(NumberOfContainerHavingMosquitoLarva);
            m.NumberOfDrainAndSoakPitChecked = Int32.Parse(NumberOfDrainAndSoakPitChecked);
            m.NumberOfDrainAndSoakPitHavingMosquitoLarva = Int32.Parse(NumberOfDrainAndSoakPitHavingMosquitoLarva);
            m.NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople = Int32.Parse(NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople);
            m.NumberOfLeafletDistribution = Int32.Parse(NumberOfLeafletDistribution);



            db.SaveChanges();


            gvH2h.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hGrid(PnchtId); 
        }

        protected void gvH2h_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvH2h.DataKeys[e.RowIndex].Values[0]);
            db.H2H.Where(s => s.h2hId == id).Delete();
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getH2hGrid(PnchtId); 
        }



        protected void ddlsearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            int w = ddlSearchWeek.SelectedIndex;
            int d = ddlSrearchDay.SelectedIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getH2hGrid(PnchtId);
        }

        protected void ddlSearchWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getH2hGrid( PnchtId);
        }

        protected void ddlSrearchDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getH2hGrid(PnchtId);
        }

        private bool btnGetFevCount()
        {
            try
            {
                int ct = db.FeverMasters.Where(s => s.H2Hid == null).Count();
                if (ct != Int32.Parse(txtNumberOfPersonHavingFeverDatewise.Text))
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected void Btnshow_Click(object sender, EventArgs e)
        {
            ModalPopupExtender1.Show();
        }

        protected void txtNumberOfPersonHavingFeverDatewise_TextChanged(object sender, EventArgs e)
        {
            Session["fCt"] = txtNumberOfPersonHavingFeverDatewise.Text;
            Btnshow_Click(null, null);

        }

        protected void gvH2h_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();


            if (e.CommandName == "ViewFever")
            {
                Response.Redirect("/fever.aspx?id=" + id);
            }
        }
    }
}