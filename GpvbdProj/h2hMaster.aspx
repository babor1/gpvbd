﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="h2hMaster.aspx.cs" Inherits="GpvbdProj.h2hMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <style>
        .inner-datepicker {
            display: inline-block;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "tab0";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>

    <style type="text/css">
        @font-face {
            font-family: "Amar Bangla Normal";
            src: url(http://www.examplefonts.com/example.ttf) format("truetype");
        }

        div.ExampleFont {
            font-family: "The Example Font", Amar Bangla Normal;
        }
    </style>
    <%--<div class="ExampleFont ">Bj¡l e¡j j¡¢qc¤m Cpm¡j</div>--%>

    <asp:HiddenField ID="TabName" runat="server" />

    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>H2H Master </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">H2H Master</li>
            </ol>
        </nav>
    </div>
    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">New</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">View</span></a> </li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane active" id="tab0" role="tabpanel">
                <%-- <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                <asp:UpdatePanel ID="gvCartUpdate" runat="server" UpdateMode="Always">
                    <ContentTemplate>--%>
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body" style="padding: .10rem 1rem;">
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">SANSAD NAME</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlSansadNo" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlSansadNo_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <%--<asp:TextBox ID="txtSansadNo" runat="server" class="form-control" placeholder="Sansad No"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">HOUSE NO</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtHouseNo" runat="server" class="form-control" placeholder="House No"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtHouseNo" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">NAME</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtName" runat="server" class="form-control" placeholder="Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">FATHER NAME</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtFatherName" runat="server" class="form-control" placeholder="Father Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtFatherName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">FAMILY MEMBER</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtFamilyMember" runat="server" class="form-control" placeholder="Family Member"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtFamilyMember" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MOBILE NO</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtMobileNo" runat="server" class="form-control" placeholder="MobileNo"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-gradient-primary mr-2" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-light" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <%-- </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>--%>
            </div>
            <div class="tab-pane p-20" id="tab1" role="tabpanel">
                <div style="margin: 10px; overflow-x: auto">
                    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvH2hUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">SANSAD NAME</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlViewSansad" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlViewSansad_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:Button ID="btnGetReoprt" runat="server" Text="Get Report" class="btn btn-gradient-primary mr-2" OnClick="btnGetReoprt_Click" />
                            <asp:GridView ID="gvH2hMaster" runat="server" DataKeyNames="id" OnRowDataBound="gvH2hMaster_RowDataBound" OnRowEditing="gvH2hMaster_RowEditing" OnRowCancelingEdit="gvH2hMaster_RowCancelingEdit" OnRowUpdating="gvH2hMaster_RowUpdating" OnRowDeleting="gvH2hMaster_RowDeleting"
                                EmptyDataText="No records has been added." AutoGenerateEditButton="true" AutoGenerateDeleteButton="true" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                    <asp:BoundField DataField="SansadName" HeaderText="Sansad Name" />
                                    <asp:BoundField DataField="HouseNo" HeaderText="House No" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
                                    <asp:BoundField DataField="FamilyMember" HeaderText="Family Member" />
                                    <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
