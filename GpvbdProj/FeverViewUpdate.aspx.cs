﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class FeverViewUpdate : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities(); 
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getFeverGridQrStr(PnchtId);
            }
        }
        private void getFeverGridQrStr(int PnchtId)
        {
            var s = db.FeverMasters.Where(l => l.PanchayetId == PnchtId && l.isSolved == false).Select(m => new
            {
                id = m.fmId,
                HouseNo = m.HouseNo,
                MobileNo = m.MobileNo,
                SansadName = m.SansadName,
                FatherName = m.FatherName,
                Name = m.Name,
                FeverPatientName = m.FeverPatientName,
                FeverFatherPatientName = m.FeverFatherPatientName,
                DayofFever = m.DayofFever,
                Area = m.Area,
                Status = m.isSolved
            }).ToList();

            gvFever.DataSource = s;
            gvFever.DataBind();

        }
        private void UpdateFever(int id)
        {
            var f = db.FeverMasters.Where(l => l.fmId == id).FirstOrDefault();
            f.isSolved = true;
            f.SolvedDate =DateTime.Parse(DateTime.Now.ToShortDateString());
            db.SaveChanges();
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getFeverGridQrStr(PnchtId);
        }
        protected void gvFever_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);
            
            switch (e.CommandName)
            {
                case "Solved":
                    UpdateFever(id);
                    break;
                default:
                    break;
            }
        }

        protected void gvFever_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    try
            //    {
            //        string id = ((Label)e.Row.FindControl("id")).Text;
            //        Label delvDate = ((Label)e.Row.FindControl("SolvedDate"));
            //        Button btnSolved = ((Button)e.Row.FindControl("btnSolved"));

            //        var f = db.FeverMasters.Where(l => l.fmId ==Int32.Parse(id)).FirstOrDefault();

            //        if (f.isSolved == true)
            //        {
            //            btnSolved.Visible = false;
            //        }


            //    }
            //    catch (Exception ex)
            //    {
            //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + ex.Message + "')", true);
            //    }
            //}
        }
    }
}