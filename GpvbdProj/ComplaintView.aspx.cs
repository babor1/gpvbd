﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class ComplaintView : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getComplainGrid(PnchtId);
            }
        }


        private void getComplainGrid(int PnchtId)
        {
            var s = db.PublicComplains.Where(l => l.PanchayetId == PnchtId).Select(m => new {
                id = m.ComplainId, MobileNo = m.Mobile, Village = m.Village, FatherName = m.FatherName,
                Name = m.Name, path = m.Path, isSolved = m.isSolved, SolvedDate = m.SolvedDate }).ToList();

            gvComplain.DataSource = s;
            gvComplain.DataBind();
        }


        protected void gvComplain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvComplain.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvComplain_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvComplain.EditIndex = e.NewEditIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getComplainGrid(PnchtId);
        }

        protected void gvComplain_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvComplain.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getComplainGrid(PnchtId);
        }

        protected void gvComplain_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvComplain.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvComplain.DataKeys[e.RowIndex].Values[0]);
            string Name = (row.Cells[2].Controls[0] as TextBox).Text;
            string FatherName = (row.Cells[3].Controls[0] as TextBox).Text;
            string Village = (row.Cells[4].Controls[0] as TextBox).Text;
            string MobileNo = (row.Cells[5].Controls[0] as TextBox).Text;

            PublicComplain m = db.PublicComplains.Single(s => s.ComplainId == id);


            m.Name = Name.ToUpper();
            m.FatherName = FatherName.ToUpper();
            m.Village = Village;
            m.Mobile = MobileNo;

            db.SaveChanges();


            gvComplain.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getComplainGrid(PnchtId);
        }

        protected void gvComplain_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvComplain.DataKeys[e.RowIndex].Values[0]);

            var imgs = from m in db.PublicComplains where m.ComplainId == id select m;
            string imName = "";
            foreach (PublicComplain b in imgs)
            {
                imName = getImageName(b.Path);
                var query = from o in Directory.GetFiles(System.IO.Path.Combine(Server.MapPath("~/ComplainImg/")), "*.*",
                SearchOption.AllDirectories)
                            let x = new FileInfo(o)
                            where x.Name == imName
                            select o;
                foreach (var item in query)
                {
                    File.Delete(item);
                }
            }
            db.PublicComplains.Where(s => s.ComplainId == id).Delete();


            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getComplainGrid(PnchtId);
        }
        private string getImageName(string path)
        {
            string[] a = new string[3];
            a = path.Split('/');
            string name = a[2];
            return name;
        }

        protected void btnGetReoprt_Click(object sender, EventArgs e)
        {
            bool isSolved = false;
            //if (ddlsearchStatus.SelectedIndex == 0)
            //    isSolved = false;
            //else
            //    isSolved = true;

            Response.Redirect("/ComplaintsReport.aspx?is=" + isSolved);
        }
    }
}