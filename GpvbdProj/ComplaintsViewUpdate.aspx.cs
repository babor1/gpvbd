﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class ComplaintsViewUpdate : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities(); 
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getComplainGrid(PnchtId);
            }
        }


        private void getComplainGrid(int PnchtId)
        {
            var s = db.PublicComplains.Where(l => l.PanchayetId == PnchtId && l.isSolved==false).Select(m => new {
                id = m.ComplainId,
                MobileNo = m.Mobile,
                Village = m.Village,
                FatherName = m.FatherName,
                Name = m.Name,
                path = m.Path,
                isSolved = m.isSolved,
                SolvedDate = m.SolvedDate
            }).ToList();

            gvComplain.DataSource = s;
            gvComplain.DataBind();
        }
        private void UpdateFever(int id)
        {
            var f = db.PublicComplains.Where(l => l.ComplainId == id).FirstOrDefault();
            f.isSolved = true;
            f.SolvedDate =DateTime.Parse(DateTime.Now.ToShortDateString());
            db.SaveChanges();
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getComplainGrid(PnchtId);
        }

        protected void gvComplain_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandArgument);

            switch (e.CommandName)
            {
                case "Solved":
                    UpdateFever(id);
                    break;
                default:
                    break;
            }
        }
    }
}