﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FeverViewUpdate.aspx.cs" Inherits="GpvbdProj.FeverViewUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Fever </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">FeverViewUpdate</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body" style="padding: .10rem 1rem;">
                    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvFevUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div style="margin: 10px; overflow-x: auto">
                                <asp:GridView ID="gvFever" runat="server" DataKeyNames="id" AutoGenerateColumns="false" Width="100%" OnRowCommand="gvFever_RowCommand"
                                    OnRowDataBound="gvFever_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                        <asp:BoundField DataField="SansadName" HeaderText="Sansad Name" />
                                        <asp:BoundField DataField="HouseNo" HeaderText="HouseNo" />
                                        <asp:BoundField DataField="FeverPatientName" HeaderText="Fever Patient Name" />
                                        <asp:BoundField DataField="FeverFatherPatientName" HeaderText="Fever Father Patient Name" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
                                        <asp:BoundField DataField="DayofFever" HeaderText="Day of Fever" />
                                        <asp:BoundField DataField="Area" HeaderText="Area" />
                                        <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                        <asp:BoundField DataField="Status" HeaderText="Is Solved?" />
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:Button ID="btnSolved" runat="server" CssClass="mdi mdi-check-circle" Text="Solved" CommandName="Solved" CommandArgument='<%# Eval("id") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <EditRowStyle CssClass="GridViewEditRow" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
