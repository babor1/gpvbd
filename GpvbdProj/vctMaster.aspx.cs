﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class vctMaster : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getSansadDDL(PnchtId);
                getVCTMasterGrid(PnchtId);

            }
        }


        private void getVCTMasterGrid(int PnchtId)
        {
            var s = db.VCTMasters.Where(l=>l.PanchayetId== PnchtId).Select(m => new { id = m.vctMId, HouseNo = m.HouseNo, MobileNo = m.MobileNo, SansadName = m.SansadName, FatherName = m.FatherName, Name = m.Name, LocationType = m.LocationType, path = m.path }).ToList();

            gvVCTMaster.DataSource = s;
            gvVCTMaster.DataBind();
        }
        private void getSansadDDL(int PnchtId)
        {
            var c = db.SansadMasters.Where(l=>l.PanchayetId== PnchtId).Select(m => new { id = m.GSId, Name = m.GramSansadName }).ToList();

            ddlSansadName.DataSource = c;
            ddlSansadName.DataValueField = "id";
            ddlSansadName.DataTextField = "name";
            ddlSansadName.DataBind();

            ddlSansadName.Items.Insert(0, new ListItem("Select", "0"));

            ddlSansadName.SelectedIndex = -1;
        }
        private void getHouseNo(int PnchtId)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            var c = db.H2HMaster.Where(s => s.GSId==gsId && s.PanchayetId== PnchtId).Select(m => new { houseName = m.HouseNo }).ToList();

            ddlHouseNo.DataSource = c;
            ddlHouseNo.DataValueField = "houseName";
            ddlHouseNo.DataTextField = "houseName";
            ddlHouseNo.DataBind();

            ddlHouseNo.Items.Insert(0, new ListItem("Select", "0"));

            ddlHouseNo.SelectedIndex = -1;

        }
        private void getRecordSelectedHouse(int PnchtId)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            string HouseNo = ddlHouseNo.SelectedItem.Text;
            var c = db.H2HMaster.Where(s =>s.GSId==gsId && s.HouseNo == HouseNo && s.PanchayetId== PnchtId).FirstOrDefault();

            txtName.Text = c.Name;
            if (c.FatherName == null)
                txtFatherName.Text = "";
            else
                txtFatherName.Text = c.FatherName;
            if (c.MobileNo == null)
                txtMobileNo.Text = "";
            else
                txtMobileNo.Text = c.MobileNo;

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ddlSansadName.SelectedItem.Text != "Select")
            {
                VCTMaster h = new VCTMaster();
                int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
                h.GSId = gsId;
                h.SansadName = ddlSansadName.SelectedItem.Text;
                h.HouseNo = ddlHouseNo.Text;
                h.Name = txtName.Text.ToUpper();
                h.FatherName = txtFatherName.Text.ToUpper();
                h.LocationType = ddlLocationType.SelectedItem.Text;
                h.MobileNo = txtMobileNo.Text;
                h.isSolved = false;

                PnchtId = Int32.Parse(Session["pc"].ToString());
                h.PanchayetId = PnchtId;

                db.VCTMasters.Add(h);
                db.SaveChanges();

                InsertImage(h.vctMId);

                this.getVCTMasterGrid(PnchtId);
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Select Sansad Name')", true);

            reset();
        }
        private void reset()
        {
            ddlSansadName.SelectedIndex = -1;
            ddlHouseNo.SelectedIndex = -1;
            txtName.Text = "";
            txtFatherName.Text = "";
            ddlLocationType.SelectedIndex = -1;
            txtMobileNo.Text = "";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }

        protected void ddlSansadName_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getHouseNo(PnchtId);
        }

        protected void ddlHouseNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getRecordSelectedHouse(PnchtId);
        }

        protected void gvVCTMaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvVCTMaster.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvVCTMaster_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvVCTMaster.EditIndex = e.NewEditIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTMasterGrid(PnchtId);
        }

        protected void gvVCTMaster_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvVCTMaster.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTMasterGrid(PnchtId);
        }

        protected void gvVCTMaster_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvVCTMaster.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvVCTMaster.DataKeys[e.RowIndex].Values[0]);
            string SansadName = (row.Cells[2].Controls[0] as TextBox).Text;
            string HouseNo = (row.Cells[3].Controls[0] as TextBox).Text;
            string Name = (row.Cells[4].Controls[0] as TextBox).Text;
            string FatherName = (row.Cells[5].Controls[0] as TextBox).Text;
            string LocationType = (row.Cells[6].Controls[0] as TextBox).Text;
            string MobileNo = (row.Cells[7].Controls[0] as TextBox).Text;

            VCTMaster m = db.VCTMasters.Single(s => s.vctMId == id);

            m.SansadName = SansadName;
            m.HouseNo = HouseNo;
            m.Name = Name.ToUpper();
            m.FatherName = FatherName.ToUpper();
            m.LocationType = LocationType;
            m.MobileNo = MobileNo;

            db.SaveChanges();


            gvVCTMaster.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTMasterGrid(PnchtId);
        }

        protected void gvVCTMaster_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvVCTMaster.DataKeys[e.RowIndex].Values[0]);

            var imgs = from m in db.VCTMasters where m.vctMId == id select m;
            string imName = "";
            foreach (VCTMaster b in imgs)
            {
                imName = getImageName(b.path);
                var query = from o in Directory.GetFiles(System.IO.Path.Combine(Server.MapPath("~/VCTMasterImg/")), "*.*",
                SearchOption.AllDirectories)
                            let x = new FileInfo(o)
                            where x.Name == imName
                            select o;
                foreach (var item in query)
                {
                    File.Delete(item);
                }
            }
            db.VCTMasters.Where(s => s.vctMId == id).Delete();


            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTMasterGrid(PnchtId);
        }
        private string getImageName(string path)
        {
            string[] a = new string[3];
            a = path.Split('/');
            string name = a[2];
            return name;
        }
        private void InsertImage(int VctMId)
        {
            try
            {
                if (fuVctMasterImage.HasFiles)
                {
                    VCTMaster bi = db.VCTMasters.Where(d => d.vctMId == VctMId).FirstOrDefault();
                    foreach (HttpPostedFile uploadedFile in fuVctMasterImage.PostedFiles)
                    {
                        uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/VCTMasterImg/"), uploadedFile.FileName));
                        bi.path = "~/VCTMasterImg/" + uploadedFile.FileName;
                    }
                }
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + ex.Message + "')", true);
            }
        }

        protected void btnGetReoprt_Click(object sender, EventArgs e)
        {
            bool isSolved = false;
            //if (ddlsearchStatus.SelectedIndex == 0)
            //    isSolved = false;
            //else
            //    isSolved = true;

            Response.Redirect("/VCTMasterReport.aspx?is=" + isSolved);
        }
    }
}