﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="h2h.aspx.cs" Inherits="GpvbdProj.h2h" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>H2H </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">H2H</li>
            </ol>
        </nav>
    </div>

    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <cc1:ToolkitScriptManager runat="server" EnablePageMethods="true">
    </cc1:ToolkitScriptManager>


    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">New</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">View</span></a> </li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane active" id="tab0" role="tabpanel">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body" style="padding: .10rem 1rem;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Btnshow"
                                            PopupControlID="Panel1">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none">
                                            <iframe style="width: 580px; height: 500px;" id="irm1" src="FeverModelForm.aspx" runat="server"></iframe>
                                            <br />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" />
                                        </asp:Panel>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MONTH</label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">WEEK</label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlWeek" runat="server" class="form-control" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                    <asp:ListItem>1</asp:ListItem>
                                                    <asp:ListItem>2</asp:ListItem>
                                                    <asp:ListItem>3</asp:ListItem>
                                                    <asp:ListItem>4</asp:ListItem>
                                                    <asp:ListItem>5</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">DAY</label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlDay" runat="server" class="form-control" OnSelectedIndexChanged="ddlDay_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                    <asp:ListItem>1</asp:ListItem>
                                                    <asp:ListItem>2</asp:ListItem>
                                                    <asp:ListItem>3</asp:ListItem>
                                                    <asp:ListItem>4</asp:ListItem>
                                                    <asp:ListItem>5</asp:ListItem>
                                                    <asp:ListItem>6</asp:ListItem>
                                                    <asp:ListItem>7</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">GRAM SANSAD NAME</label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlSansadName" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlSansadName_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">District ID</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDistrictId" runat="server" class="form-control" placeholder="District Id" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Block ID</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtBlockId" runat="server" class="form-control" placeholder="Block Id" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Gram Panchayat Id</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtGramPanchayatId" runat="server" class="form-control" placeholder="Gram Panchayat Id" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Gram Sansad Id</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtGramSansadId" runat="server" class="form-control" placeholder="Gram Sansad Id" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Population Of Area Visited</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtPopulationOfAreaVisited" runat="server" class="form-control" placeholder="Population Of Area Visited"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Households Visited Families</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfHouseholdsVisitedFamilies" runat="server" class="form-control" placeholder="Number Of Households Visited Families"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Person Having Fever Datewise</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfPersonHavingFeverDatewise" OnTextChanged="txtNumberOfPersonHavingFeverDatewise_TextChanged" runat="server" class="form-control" placeholder="Number Of Person Having Fever Datewise" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Container Checked</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfContainerChecked" runat="server" class="form-control" placeholder="Number Of Container Checked"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Container Having Mosquito Larva</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfContainerHavingMosquitoLarva" runat="server" class="form-control" placeholder="Number Of Container Having Mosquito Larva"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Drain And Soak Pit Checked</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfDrainAndSoakPitChecked" runat="server" class="form-control" placeholder="Number Of Drain And Soak Pit Checked"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Drain And Soak Pit Having Mosquito Larva</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfDrainAndSoakPitHavingMosquitoLarva" runat="server" class="form-control" placeholder="Number Of Drain And Soak Pit Having Mosquito Larva"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Container Drain And Soak Pit Managed VRP or Common People</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople" runat="server" class="form-control" placeholder="Number Of Container Drain And Soak Pit Managed VRP or Common People"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Number Of Leaflet Distribution</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumberOfLeafletDistribution" runat="server" class="form-control" placeholder="Number Of Leaflet Distribution"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:Button ID="Btnshow" runat="server" Text="" OnClick="Btnshow_Click" Enabled="false" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtNumberOfPersonHavingFeverDatewise" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-gradient-primary mr-2" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-light" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane p-20" id="tab1" role="tabpanel">
                <div style="margin: 10px;">

                    <asp:UpdatePanel ID="gvH2hUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MONTH</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlsearch" runat="server" class="form-control" OnSelectedIndexChanged="ddlsearch_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">WEEK</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlSearchWeek" runat="server" class="form-control" OnSelectedIndexChanged="ddlSearchWeek_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem>Select</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">DAY</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlSrearchDay" runat="server" class="form-control" OnSelectedIndexChanged="ddlSrearchDay_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem>Select</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>6</asp:ListItem>
                                        <asp:ListItem>7</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="margin: 10px; overflow-x: auto">
                                <asp:GridView ID="gvH2h" runat="server" DataKeyNames="id" OnRowDataBound="gvH2h_RowDataBound" OnRowEditing="gvH2h_RowEditing" OnRowCancelingEdit="gvH2h_RowCancelingEdit" OnRowUpdating="gvH2h_RowUpdating" OnRowDeleting="gvH2h_RowDeleting"
                                    EmptyDataText="No records has been added." OnRowCommand="gvH2h_RowCommand" AutoGenerateEditButton="true" AutoGenerateDeleteButton="true" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                        <asp:BoundField DataField="SansadName" HeaderText="Sansad Name" />
                                        <asp:BoundField DataField="SansadId" HeaderText="Sansad Id" />
                                        <asp:BoundField DataField="PopulationOfAreaVisited" HeaderText="Popl. Area Visited" />
                                        <asp:BoundField DataField="NumberOfHouseholdsVisitedFamilies" HeaderText="No. Households Visited Families" />
                                        <%--<asp:BoundField DataField="NumberOfPersonHavingFeverDatewise" HeaderText="No. Person Having Fever Datewise" />--%>
                                        <asp:TemplateField HeaderText="No. Person Having Fever Datewise">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="NumberOfPersonHavingFeverDatewise" CommandName="ViewFever" CommandArgument='<%# Eval("id") %>' runat="server"><%# Eval("NumberOfPersonHavingFeverDatewise") %></asp:LinkButton>
                                                <%--<asp:Button ID="NumberOfPersonHavingFeverDatewise" runat="server" Text='<%# Eval("NumberOfPersonHavingFeverDatewise") %>' CommandName="ViewFever" CommandArgument='<%# Eval("id") %>' />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="NumberOfContainerChecked" HeaderText="No. Container Checked" />
                                        <asp:BoundField DataField="NumberOfContainerHavingMosquitoLarva" HeaderText="No. Container Having Mosquito Larva" />
                                        <asp:BoundField DataField="NumberOfDrainAndSoakPitChecked" HeaderText="No. Drain-Soak Pit Checked" />
                                        <asp:BoundField DataField="NumberOfDrainAndSoakPitHavingMosquitoLarva" HeaderText="No. Drain-Soak Pit Having Mosquito Larva" />
                                        <asp:BoundField DataField="NumberOfContainerDrainAndSoakPitManagedVRPorCommonPeople" HeaderText="No. Container Drain-Soak Pit Managed VRP_CommonPeople" />
                                        <asp:BoundField DataField="NumberOfLeafletDistribution" HeaderText="No. Leaflet Distribution" />
                                    </Columns>
                                    <EditRowStyle CssClass="GridViewEditRow" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%-- <script lang="javascript" type="text/javascript">
        function onModalOk() {
            document.getElementById("<%=txtNumberOfPersonHavingFeverDatewise.ClientID%>").value = "";
            var inf =<%=Session["fCnt"]%>;
            if (inf != null) {
                document.getElementById("<%=txtNumberOfPersonHavingFeverDatewise.ClientID%>").value = <%=Session["fCnt"]%>;
            }
        }
    </script>
    <script>
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    function onModalOk() {
                        document.getElementById("<%=txtNumberOfPersonHavingFeverDatewise.ClientID%>").value = "";
                        var inf =<%=Session["fCnt"]%>;
                        if (inf != null) {
                            document.getElementById("<%=txtNumberOfPersonHavingFeverDatewise.ClientID%>").value = <%=Session["fCnt"]%>;
                        }
                    }

                }
            });

        };
    </script>--%>
</asp:Content>
