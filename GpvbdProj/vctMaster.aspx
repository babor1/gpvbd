﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="vctMaster.aspx.cs" Inherits="GpvbdProj.vctMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>VCT Master </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">VCT Master</li>
            </ol>
        </nav>
    </div>
    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">New</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">View</span></a> </li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane active" id="tab0" role="tabpanel">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body" style="padding: .10rem 1rem;">
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">SANSAD NAME</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlSansadName" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlSansadName_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">HOUSE NO</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlHouseNo" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlHouseNo_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">NAME</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtName" runat="server" class="form-control" placeholder="Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;" ControlToValidate="txtName"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtName" SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">FATHER NAME</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtFatherName" runat="server" class="form-control" placeholder="Father Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtFatherName" SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Location Type</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlLocationType" runat="server" class="select2 form-control custom-select">
                                            <asp:ListItem>Select</asp:ListItem>
                                            <asp:ListItem>Garbage Piles</asp:ListItem>
                                            <asp:ListItem>Blocked Drain</asp:ListItem>
                                            <asp:ListItem>Low Land</asp:ListItem>
                                            <asp:ListItem>Tyre Repairing</asp:ListItem>
                                            <asp:ListItem>Recycling Factory</asp:ListItem>
                                            <asp:ListItem>Unsafe Constraction</asp:ListItem>
                                            <asp:ListItem>Canal Ditch Cesspool</asp:ListItem>
                                            <asp:ListItem>Chamber/Shokpit</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MOBILE NO</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtMobileNo" runat="server" class="form-control" placeholder="MobileNo"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">
                                        <asp:FileUpload ID="fuVctMasterImage" runat="server" CssClass="form-control"></asp:FileUpload></label>
                                    <div class="col-sm-9" style="text-align: center;">
                                        <div id="dvPreview">
                                        </div>
                                    </div>
                                </div>
                                <p>
                                </p>
                                <br />
                                <br />

                                <div class="form-group">
                                    <asp:DataList ID="dlVCTMastrImage" RepeatColumns="5" CellPadding="2" runat="server">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidImageId" runat="server" Value='<%# Eval("Id") %>' />
                                            <div class="">
                                                <asp:Image ID="Image1" runat="server" Width="100px" Height="120px" ImageUrl='<%# Eval("path") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-gradient-primary mr-2" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-light" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="tab1" role="tabpanel">
                <div style="margin: 10px; overflow-x: auto">
                    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvVCTUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Button ID="btnGetReoprt" runat="server" Text="Get Report" class="btn btn-gradient-primary mr-2" OnClick="btnGetReoprt_Click" />
                            <asp:GridView ID="gvVCTMaster" runat="server" DataKeyNames="id" OnRowDataBound="gvVCTMaster_RowDataBound" OnRowEditing="gvVCTMaster_RowEditing" OnRowCancelingEdit="gvVCTMaster_RowCancelingEdit" OnRowUpdating="gvVCTMaster_RowUpdating" OnRowDeleting="gvVCTMaster_RowDeleting"
                                EmptyDataText="No records has been added." AutoGenerateEditButton="true" AutoGenerateDeleteButton="true" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                    <asp:BoundField DataField="SansadName" HeaderText="Sansad Name" />
                                    <asp:BoundField DataField="HouseNo" HeaderText="House No" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
                                    <asp:BoundField DataField="LocationType" HeaderText="Location Type" />
                                    <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                    <asp:TemplateField HeaderText="Image">
                                        <ItemTemplate>
                                            <asp:Image ID="Img" runat="server" Width="70px" Height="95px" ImageUrl='<%# Eval("path") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle CssClass="GridViewEditRow" />
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.2.1.min.js">
    </script>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
            var fileUpload = document.getElementById("MainContent_fuVctMasterImage");
            fileUpload.onchange = function () {
                if (typeof (FileReader) != "undefined") {
                    var dvPreview = document.getElementById("dvPreview");
                    dvPreview.innerHTML = "";
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                    for (var i = 0; i < fileUpload.files.length; i++) {
                        var file = fileUpload.files[i];
                        if (regex.test(file.name.toLowerCase())) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var img = document.createElement("IMG");
                                img.height = "100";
                                img.width = "100";
                                img.src = e.target.result;
                                dvPreview.appendChild(img);
                            }
                            reader.readAsDataURL(file);
                        } else {
                            alert(file.name + " is not a valid image file.");
                            dvPreview.innerHTML = "";
                            return false;
                        }
                    }
                } else {
                    alert("This browser does not support HTML5 FileReader.");
                }
            }
        };
    </script>
</asp:Content>
