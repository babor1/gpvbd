﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GpvbdProj.Model;


namespace GpvbdProj
{
    public partial class SiteMaster : MasterPage
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getPanchayet();
                if (Session["pc"] != null)
                {
                    ddlPanchayet.SelectedValue = Session["pc"].ToString();
                    getNotification();
                }
                else
                    Session["pc"] = 1;
            }
        }

        private void getPanchayet()
        {
            var a = db.PanchayetMsters.Where(l=>l.PanchayetId==1).ToList();
            ddlPanchayet.DataSource = a;
            ddlPanchayet.DataTextField = "Name";
            ddlPanchayet.DataValueField = "PanchayetId";
            ddlPanchayet.DataBind();
        }

        private void getNotification()
        {
            PnchId = Int32.Parse(Session["pc"].ToString());
            Notification nt = new Notification();
            labFevCount.Text = nt.getFeverNotf(PnchId);

            labComplaintsCount.Text = nt.getComplaintNotf(PnchId);

           
        }
        protected void ddlPanchayet_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["pc"] = ddlPanchayet.SelectedValue;
            getNotification();
            Response.Redirect("/Default.aspx");
        }
    }
}