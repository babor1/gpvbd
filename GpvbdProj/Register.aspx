﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="GpvbdProj.Register" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GPVBD Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../../assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../assets/images/favicon.png" />
</head>
<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth">
                <div class="row flex-grow">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left p-5">
                            <div class="brand-logo">
                                <img src="../../assets/images/logohub.png">
                            </div>
                            <h4>New here?</h4>
                            <h6 class="font-weight-light">Signing up is easy. It only takes a few steps</h6>
                            <form class="pt-3" runat="server">
                                <div class="form-group">
                                    <asp:TextBox ID="txtName" class="form-control form-control-lg" required placeholder="Name" runat="server" />
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtMobileNo" class="form-control form-control-lg" required placeholder="MobileNo/UserName" runat="server" />
                                </div>
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlPanchayet" class="select2 form-control custom-select" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlUserType" class="select2 form-control custom-select" runat="server">
                                        <asp:ListItem Text="Select User Type" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Administrator" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="System User" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Operator" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" class="form-control form-control-lg" required placeholder="Password" runat="server" />
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtAdminPassword" TextMode="Password" class="form-control form-control-lg" required placeholder="AdminPassword" runat="server" />
                                </div>
                                <div class="mb-4">
                                    <div class="form-check">
                                        <%--<label class="form-check-label text-muted">
                                            <input type="checkbox" class="form-check-input">
                                            I agree to all Terms & Conditions
                                        </label>--%>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <%--<a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html">SIGN UP</a>--%>
                                    <asp:Button ID="btnSignUp" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" runat="server" Text="SIGN UP" OnClick="btnSignUp_Click"/>
                                </div>
                                <div class="text-center mt-4 font-weight-light">
                                    Already have an account? <a href="Login.aspx" class="text-primary">Login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="../../assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="../../assets/js/off-canvas.js"></script>
    <script src="../../assets/js/hoverable-collapse.js"></script>
    <script src="../../assets/js/misc.js"></script>
    <!-- endinject -->
</body>
</html>
