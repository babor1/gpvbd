﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="fever.aspx.cs" Inherits="GpvbdProj.fever" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>Fever </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item"><a href="/h2h.aspx">H2H</a></li>
                <li class="breadcrumb-item active" aria-current="page">Fever</li>
            </ol>
        </nav>
    </div>
    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">New</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">View</span></a> </li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane active" id="tab0" role="tabpanel">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body" style="padding: .10rem 1rem;">
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">SANSAD NAME</label>
                                    <div class="col-sm-5 fev">
                                        <asp:DropDownList ID="ddlSansadName" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlSansadName_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">HOUSE NO</label>
                                    <div class="col-sm-5 fev">
                                        <asp:DropDownList ID="ddlHouseNo" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlHouseNo_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">FEVER PATIENT NAME</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtFevPtName" runat="server" class="form-control" placeholder="Fever Patient Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">FEVER PATIENT FATHER NAME</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtFevPtFatherName" runat="server" class="form-control" placeholder="Fever Patient Father Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">NAME</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtName" runat="server" class="form-control" placeholder="Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">FATHER NAME</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtFatherName" runat="server" class="form-control" placeholder="Father Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">DAY OF FEVER</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtDayOfFever" runat="server" class="form-control" placeholder="Day Of Fever"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MOBILE NO</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtMobileNo" runat="server" class="form-control" placeholder="MobileNo"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">AREA</label>
                                    <div class="col-sm-5 fev">
                                        <asp:TextBox ID="txtArea" runat="server" class="form-control" placeholder="Area" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                                    </div>
                                </div>
                                <%--<div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Fever Patient Submited</label>
                                    <div class="col-sm-5 fev">
                                        <asp:Label ID="labDisp" runat="server" Text="0"></asp:Label>
                                    </div>
                                </div>--%>

                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-gradient-primary mr-2" OnClick="btnSubmit_Click" Style="margin-left: 200px;" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-light" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="tab1" role="tabpanel">
                <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                <asp:UpdatePanel ID="gvFevUpdate" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div style="margin: 10px;">
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">STATUS</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlsearchStatus" runat="server" class="form-control" OnSelectedIndexChanged="ddlsearchStatus_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem>Pending</asp:ListItem>
                                        <asp:ListItem>Solved</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btnGetReoprt" runat="server" Text="Get Report" class="btn btn-gradient-primary mr-2" OnClick="btnGetReoprt_Click" />
                                </div>
                            </div>
                        </div>


                        <div style="margin: 10px; overflow-x: auto">
                            <asp:GridView ID="gvFever" runat="server" DataKeyNames="id" OnRowDataBound="gvFever_RowDataBound" OnRowEditing="gvFever_RowEditing" OnRowCancelingEdit="gvFever_RowCancelingEdit" OnRowUpdating="gvFever_RowUpdating" OnRowDeleting="gvFever_RowDeleting"
                                EmptyDataText="No records has been added." AutoGenerateEditButton="true" AutoGenerateDeleteButton="true" AutoGenerateColumns="false" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                    <asp:BoundField DataField="SansadName" HeaderText="Sansad Name" />
                                    <asp:BoundField DataField="HouseNo" HeaderText="HouseNo" />
                                    <asp:BoundField DataField="FeverPatientName" HeaderText="Fever Patient Name" />
                                    <asp:BoundField DataField="FeverFatherPatientName" HeaderText="Fever Patient Father Name" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
                                    <asp:BoundField DataField="DayofFever" HeaderText="Day of Fever" />
                                    <asp:BoundField DataField="Area" HeaderText="Area" />
                                    <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                    <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" />
                                    <asp:BoundField DataField="Status" HeaderText="Is Solved?" />
                                    <asp:BoundField DataField="SolvedDate" HeaderText="Solved Date" />
                                </Columns>
                                <EditRowStyle CssClass="GridViewEditRow" />
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
            </div>

        </div>
    </div>
    <%-- <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body" style="padding: .10rem 1rem;">
                    <div style="margin: 10px;">
                        <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>

                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
