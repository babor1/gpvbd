﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using GpvbdProj.Reports;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace GpvbdProj
{
    public partial class ComplaintsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                getComplaintsReport();
            }
        }
        public void getComplaintsReport()
        {
            try
            {
                this.ReportViewer1.LocalReport.EnableExternalImages = true;
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ComplaintsRpt.rdlc");

                ComplaintsDSClass tr = new ComplaintsDSClass();
                DataSet ds = new DataSet();

                bool islv = bool.Parse(Request.QueryString["is"]);
                ds = tr.getFever(true);

                string path = new Uri(Server.MapPath("~/ComplainImg")).AbsoluteUri; // adjust path to Project folder here

                // set above path to report parameter
                var parameter = new ReportParameter[3];
                parameter[0] = new ReportParameter("ImagePath", path); // adjust parameter name here
                parameter[1] = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));
                string pc = "";
                if (Int32.Parse(Session["pc"].ToString()) == 1)
                    pc = "RADHARGHAT-1";
                else
                    pc = "SAHAJADPUR";

                parameter[2] = new ReportParameter("Panchayet", pc);
                ReportViewer1.LocalReport.SetParameters(parameter);

                //ReportParameter rpDate = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));

                ReportDataSource datasource = new ReportDataSource("CompliantsDS", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                //ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rpDate });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }
    }
}