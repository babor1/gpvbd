﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="vct.aspx.cs" Inherits="GpvbdProj.vct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-header" style="width: 100%;">
        <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
            </span>VCT </h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Default.aspx">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">VCT</li>
            </ol>
        </nav>
    </div>
    <div class="card" id="Tabs">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab0" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">New</span></a> </li>
            <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#tab1" role="tab"><span class="hidden-sm-up"></span><span class="hidden-xs-down">View</span></a> </li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content tabcontent-border">
            <div class="tab-pane active" id="tab0" role="tabpanel">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body" style="padding: .10rem 1rem;">
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MONTH</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">WEEK</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlWeek" runat="server" class="form-control">
                                            <asp:ListItem>Select</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">DAY</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlDay" runat="server" class="form-control">
                                            <asp:ListItem>Select</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">GRAM SANSAD NAME</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlSansadName" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlSansadName_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <asp:Panel runat="server" ID="pnlAutoDetail" Visible="false">
                                    <div class="form-group row">
                                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">District ID</label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtDistrictId" runat="server" class="form-control" placeholder="District Id" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Block ID</label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtBlockId" runat="server" class="form-control" placeholder="Block Id" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Gram Panchayat Id</label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtGramPanchayatId" runat="server" class="form-control" placeholder="Gram Panchayat Id" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Gram Sansad Id</label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtGramSansadId" runat="server" class="form-control" placeholder="Gram Sansad Id" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">NoOfGarbagePilesDetected</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfGarbagePilesDetected" runat="server" class="form-control" placeholder="No Of Garbage Piles Detected"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Garbage Piles Managed</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfGarbagePilesManaged" runat="server" class="form-control" placeholder="No Of Garbage Piles Managed"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Blocked Drain Detected</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfBlockedDrainDetected" runat="server" class="form-control" placeholder="No Of Blocked Drain Detected"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Blocked Drain Managed</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfBlockedDrainManaged" runat="server" class="form-control" placeholder="No Of Blocked Drain Managed"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Low Land With Stagnant Water Detected</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfLowLandWithStagnantWaterDetected" runat="server" class="form-control" placeholder="No Of Low Land With Stagnant Water Detected"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Low Land With Stagnant Water Managed</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfLowLandWithStagnantWaterManaged" runat="server" class="form-control" placeholder="No Of Low Land With Stagnant Water Managed"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Tyre Repairing Recycling Factory Dectected</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfTyreRepairingRecyclingFactoryDectected" runat="server" class="form-control" placeholder="No Of Tyre Repairing Recycling Factory Dectected"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Tyre Repairing Recycling Factory Managed</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfTyreRepairingRecyclingFactoryManaged" runat="server" class="form-control" placeholder="No Of Tyre Repairing Recycling Factory Managed"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Unsafe Constraction Site Detected</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfUnsafeConstractionSiteDetected" runat="server" class="form-control" placeholder="No Of Unsafe Constraction Site Detected"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Unsafe Constraction Site Managed</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfUnsafeConstractionSiteManaged" runat="server" class="form-control" placeholder="No Of Unsafe Constraction Site Managed"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Canal Ditch Cesspool Found To Have Mosquito Larvae</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae" runat="server" class="form-control" placeholder="No Of Canal Ditch Cesspool Found To Have Mosquito Larvae"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">No Of Canal Ditch Cesspool Managed Mosquito Larvae</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtNoOfCanalDitchCesspoolManagedMosquitoLarvae" runat="server" class="form-control" placeholder="No Of Canal Ditch Cesspool Managed Mosquito Larvae"></asp:TextBox>
                                    </div>
                                </div>

                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-gradient-primary mr-2" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-light" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane p-20" id="tab1" role="tabpanel">
                <div style="margin: 10px;">
                    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel ID="gvH2hUpdate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">MONTH</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlSearchMonth" runat="server" class="form-control" OnSelectedIndexChanged="ddlSearchMonth_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">WEEK</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlSearchWeek" runat="server" class="form-control" OnSelectedIndexChanged="ddlSearchWeek_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem>Select</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">DAY</label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlSrearchDay" runat="server" class="form-control" OnSelectedIndexChanged="ddlSrearchDay_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem>Select</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>6</asp:ListItem>
                                        <asp:ListItem>7</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div style="margin: 10px; overflow-x: auto">
                                <asp:GridView ID="gvVCT" runat="server" DataKeyNames="id" OnRowDataBound="gvVCT_RowDataBound" OnRowEditing="gvVCT_RowEditing" OnRowCancelingEdit="gvVCT_RowCancelingEdit" OnRowUpdating="gvVCT_RowUpdating" OnRowDeleting="gvVCT_RowDeleting"
                                    EmptyDataText="No records has been added." AutoGenerateEditButton="true" AutoGenerateDeleteButton="true" AutoGenerateColumns="false" Width="100%"
                                    AllowPaging="true" OnPageIndexChanging="gvVCT_PageIndexChanging" PageSize="10">
                                    <Columns>
                                        <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                                        <asp:BoundField DataField="SansadName" HeaderText="Sansad Name" />
                                        <asp:BoundField DataField="SansadId" HeaderText="Sansad Id" />
                                        <asp:BoundField DataField="NoOfGarbagePilesDetected" HeaderText="No. GarbagePilesDetected" />
                                        <asp:BoundField DataField="NoOfGarbagePilesManaged" HeaderText="No. Garbage Piles Managed" />
                                        <asp:BoundField DataField="NoOfBlockedDrainDetected" HeaderText="No. Blocked Drain Detected" />
                                        <asp:BoundField DataField="NoOfBlockedDrainManaged" HeaderText="No. BlockedDrainManaged" />
                                        <asp:BoundField DataField="NoOfLowLandWithStagnantWaterDetected" HeaderText="No. Low Land With Stagnant Water Detected" />
                                        <asp:BoundField DataField="NoOfLowLandWithStagnantWaterManaged" HeaderText="No. Low Land With Stagnant Water Managed" />
                                        <asp:BoundField DataField="NoOfTyreRepairingRecyclingFactoryDectected" HeaderText="No. Tyre Repairing Recycling Factory Dectected" />
                                        <asp:BoundField DataField="NoOfTyreRepairingRecyclingFactoryManaged" HeaderText="No. Tyre Repairing Recycling Factory Managed" />
                                        <asp:BoundField DataField="NoOfUnsafeConstractionSiteDetected" HeaderText="No. Unsafe Constraction Site Detected" />
                                        <asp:BoundField DataField="NoOfUnsafeConstractionSiteManaged" HeaderText="No. Unsafe Constraction Site Managed" />
                                        <asp:BoundField DataField="NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae" HeaderText="No. Canal Ditch Cesspool Found To Have Mosquito Larvae" />
                                        <asp:BoundField DataField="NoOfCanalDitchCesspoolManagedMosquitoLarvae" HeaderText="No. Canal Ditch Cesspool Managed MosquitoL arvae" />
                                    </Columns>
                                    <EditRowStyle CssClass="GridViewEditRow" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
