﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class FeverModelForm : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getSansadDDL(PnchtId);
            }
        }
        private void getSansadDDL(int PnchtId)
        {
            var c = db.SansadMasters.Where(l=>l.PanchayetId== PnchtId).Select(m => new { id = m.GSId, Name = m.GramSansadName }).ToList();

            ddlSansadName.DataSource = c;
            ddlSansadName.DataValueField = "id";
            ddlSansadName.DataTextField = "name";
            ddlSansadName.DataBind();

            ddlSansadName.Items.Insert(0, new ListItem("Select", "0"));

            ddlSansadName.SelectedIndex = -1;
        }
        private void getHouseNo(int PnchtId)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            var c = db.H2HMaster.Where(s => s.GSId==gsId && s.PanchayetId== PnchtId).Select(m => new { houseName = m.HouseNo }).ToList();

            ddlHouseNo.DataSource = c;
            ddlHouseNo.DataValueField = "houseName";
            ddlHouseNo.DataTextField = "houseName";
            ddlHouseNo.DataBind();

            ddlHouseNo.Items.Insert(0, new ListItem("Select", "0"));

            ddlHouseNo.SelectedIndex = -1;

        }
        private void getRecordSelectedHouse(int PnchtId)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            string HouseNo = ddlHouseNo.SelectedItem.Text;
            var c = db.H2HMaster.Where(s =>s.GSId==gsId && s.HouseNo == HouseNo && s.PanchayetId== PnchtId).FirstOrDefault();

            txtName.Text = c.Name;
            if (c.FatherName == null)
                txtFatherName.Text = "";
            else
                txtFatherName.Text = c.FatherName;
            if (c.MobileNo == null)
                txtMobileNo.Text = "";
            else
                txtMobileNo.Text = c.MobileNo;

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int c = 0;
            try
            {
                c = Int32.Parse(Session["fCt"].ToString());
            }
            catch (Exception)
            {


            }
            if (Int32.Parse(labDisp.Text) <= c)
            {
                if (ddlSansadName.SelectedItem.Text != "Select")
                {
                    FeverMaster h = new FeverMaster();
                    h.SansadName = ddlSansadName.SelectedItem.Text;
                    h.HouseNo = ddlHouseNo.Text;
                    int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
                    h.GSId = gsId;
                    h.FeverPatientName = txtFevPtName.Text.ToUpper();
                    h.FeverFatherPatientName = txtFevPtFatherName.Text.ToUpper();
                    h.Name = txtName.Text.ToUpper();
                    h.FatherName = txtFatherName.Text.ToUpper();
                    h.DayofFever = txtDayOfFever.Text;
                    h.Area = txtArea.Text.ToUpper();
                    h.MobileNo = txtMobileNo.Text;
                    PnchtId = Int32.Parse(Session["pc"].ToString());
                    h.PanchayetId = PnchtId;

                    db.FeverMasters.Add(h);
                    db.SaveChanges();

                    int cnt = Int32.Parse(labDisp.Text) + 1;
                    labDisp.Text = cnt.ToString();


                }
                else
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Select Sansad Name')", true);

            }

            reset();
        }
        private void reset()
        {
            //ddlSansadName.SelectedIndex = -1;
            //ddlHouseNo.SelectedIndex = -1;
            //txtName.Text = "";
            // txtFatherName.Text = "";
            txtArea.Text = "";
            txtFevPtFatherName.Text = "";
            txtFevPtName.Text = "";
            //txtMobileNo.Text = "";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }

        protected void ddlSansadName_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getHouseNo(PnchtId);
        }

        protected void ddlHouseNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getRecordSelectedHouse(PnchtId);
        }
    }
}