﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TestModelPopUp.aspx.cs" Inherits="GpvbdProj.TestModelPopUp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <cc1:ToolkitScriptManager runat="server">
</cc1:ToolkitScriptManager>
<asp:Button ID="btnShow" runat="server" Text="Show Modal Popup" />
<!-- ModalPopupExtender -->
<cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="btnShow"
    CancelControlID="btnClose" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none">
    <iframe style=" width: 100%; height: 400px;" id="irm1" src="FeverModelForm.aspx" runat="server"></iframe>  
    <br />
    <asp:Button ID="btnClose" runat="server" Text="Close" />
</asp:Panel>
</asp:Content>
