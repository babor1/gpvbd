﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class Register : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getPanchayet();
            }
        }
        private void getPanchayet()
        {
            var a = db.PanchayetMsters.ToList();
            ddlPanchayet.DataSource = a;
            ddlPanchayet.DataTextField = "Name";
            ddlPanchayet.DataValueField = "PanchayetId";
            ddlPanchayet.DataBind();

            ddlPanchayet.Items.Insert(0, new ListItem("Select Panchayet", "0"));
            ddlPanchayet.SelectedIndex = -1;
        }
        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            if(db.portalUsers.Where(l=>l.uName==txtMobileNo.Text).FirstOrDefault()!=null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User is alredy registerd !!!')", true);
                return;
            }

            if(txtAdminPassword.Text=="" || txtAdminPassword.Text!="bamagp@123")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Not allow to create a new user wothout valid Admin Password !!!')", true);
                return;
            }
            portalUser pu = new portalUser();

            pu.uName = txtMobileNo.Text;
            pu.uPass = txtPassword.Text;
            pu.PanchayetId = Int32.Parse(ddlPanchayet.SelectedItem.Value);
            pu.uCategory= Int32.Parse(ddlUserType.SelectedItem.Value);
            pu.ContactNo = txtMobileNo.Text;

            db.portalUsers.Add(pu);
            db.SaveChanges();

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Register is Success.')", true);

        }
    }
}