﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeverModelForm.aspx.cs" Inherits="GpvbdProj.FeverModelForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />

    <!-- Searcj DDL -->
    <link rel="stylesheet" type="text/css" href="../../Content/css/select2.css">
    <link rel="stylesheet" type="text/css" href="../../Content/css/select2.min.css">
    <title></title>
    <style type="text/css">
        .mrg {
            margin: 0;
            width: 100%;
        }

        .fev {
            margin-bottom: 1px;
            margin-left: -25px;
            left: 140px;
            padding-left: 0;
        }

        .pad {
            padding-bottom: 0px;
            padding-right: 0;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body" style="padding: 2rem 1rem;" style="margin: -45px; overflow: hidden; margin-top: -10px; padding-bottom: 1px; padding-right: 0px;">
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">SANSAD NAME</label>
                            <div class="col-sm-4 fev">
                                <asp:DropDownList ID="ddlSansadName" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlSansadName_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">HOUSE NO</label>
                            <div class="col-sm-4 fev">
                                <asp:DropDownList ID="ddlHouseNo" class="select2 form-control custom-select" runat="server" OnSelectedIndexChanged="ddlHouseNo_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">FEVER PATIENT NAME</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtFevPtName" runat="server" class="form-control" required placeholder="Fever Patient Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">FEVER PATIENT FATHER NAME</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtFevPtFatherName" runat="server" required class="form-control" placeholder="Fever Patient Father Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">NAME</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtName" runat="server" class="form-control" required placeholder="Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">FATHER NAME</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtFatherName" runat="server" class="form-control" required placeholder="Father Name" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">DAY OF FEVER</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtDayOfFever" runat="server" class="form-control" reuired placeholder="Day Of Fever"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">MOBILE NO</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtMobileNo" runat="server" class="form-control" placeholder="MobileNo"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">AREA</label>
                            <div class="col-sm-4 fev">
                                <asp:TextBox ID="txtArea" runat="server" class="form-control" placeholder="Area" onkeyup="this.value = this.value.toLowerCase();" Style="text-transform: uppercase;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row mrg">
                            <label for="exampleInputUsername2" class="col-sm-4 fev pad">No Of Fever Patient Submited</label>
                            <div class="col-sm-4 fev">
                                <asp:Label ID="labDisp" runat="server" Text="0"></asp:Label>
                            </div>
                        </div>

                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-gradient-primary mr-2" OnClick="btnSubmit_Click" Style="margin-left: 200px;" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-light" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="assets/vendors/chart.js/Chart.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/hoverable-collapse.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/todolist.js"></script>
    <!-- End custom js for this page -->

    <!-- Search DDL Page JS -->

    <script src="../../Scripts/select2.js"></script>
    <script src="../../Scripts/select2.min.js"></script>

    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();
    </script>

    <script>
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    //***********************************//
                    // For select 2
                    //***********************************//
                    $(".select2").select2();
                }
            });

        };
    </script>
</body>
</html>
