﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using GpvbdProj.Reports;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace GpvbdProj
{
    public partial class FeverReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                getInvoiceReport();
            }
        }
        public void getInvoiceReport()
        {
            try
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/Fever.rdlc");

                FeverDSClass tr = new FeverDSClass();
                DataSet ds = new DataSet();

                bool islv = bool.Parse(Request.QueryString["is"]);
                ds = tr.getFever(islv);

                ReportParameter rpDate = new ReportParameter("ReportDate", DateTime.Now.ToString("dd-MM-yyy"));

                ReportDataSource datasource = new ReportDataSource("FeverDS", ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rpDate });
                ReportViewer1.LocalReport.DataSources.Add(datasource);

                ReportViewer1.LocalReport.Refresh();
            }
            catch (Exception ex)
            {

                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('" + ex.Message + "')</SCRIPT>");
            }
        }
    }
}