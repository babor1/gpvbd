﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class vct : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        int PnchtId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getSansadDDL(PnchtId);
                getDDLMonth();
                getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
            }
        }
        private void getDDLMonth()
        {
            for (int i = 0; i < 3; i++)
            {
                ddlMonth.Items.Add(Convert.ToDateTime(DateTime.Now.AddMonths(-i).ToShortDateString()).ToString("MMM") + "/" + Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("yyyy"));
                ddlSearchMonth.Items.Add(Convert.ToDateTime(DateTime.Now.AddMonths(-i).ToShortDateString()).ToString("MMM") + "/" + Convert.ToDateTime(DateTime.Now.ToShortDateString()).ToString("yyyy"));
            }
            ddlMonth.Items.Insert(0, new ListItem("Select", "0"));
            ddlMonth.SelectedIndex = -1;

            ddlSearchMonth.Items.Insert(0, new ListItem("Select", "0"));
            ddlSearchMonth.SelectedIndex = -1;

        }
        private void getSansadDDL(int PnchtId)
        {
            var c = db.SansadMasters.Where(l => l.PanchayetId == PnchtId).Select(m => new { id = m.GSId, Name = m.GramSansadName }).ToList();

            ddlSansadName.DataSource = c;
            ddlSansadName.DataValueField = "id";
            ddlSansadName.DataTextField = "name";
            ddlSansadName.DataBind();

            ddlSansadName.Items.Insert(0, new ListItem("Select", "0"));

            ddlSansadName.SelectedIndex = -1;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ddlSansadName.SelectedItem.Text != "Select" && ddlMonth.SelectedItem.Text != "Select" && ddlWeek.SelectedItem.Text != "Select" && ddlDay.SelectedItem.Text != "Select")
            {
                VCT h = new VCT();
                int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
                h.GSId = gsId;
                h.month = ddlMonth.SelectedItem.Text;
                h.week = Int32.Parse(ddlWeek.SelectedValue);
                h.day = Int32.Parse(ddlDay.SelectedValue);
                h.GramSansadName = ddlSansadName.SelectedItem.Text;
                h.BlockId = Int32.Parse(txtBlockId.Text);
                h.DistrictId = Int32.Parse(txtDistrictId.Text);
                h.GramPanchayatId = Int32.Parse(txtGramPanchayatId.Text);
                h.GramSansadId = Int32.Parse(txtGramSansadId.Text);

                h.NoOfGarbagePilesDetected = Int32.Parse(txtNoOfGarbagePilesDetected.Text);
                h.NoOfGarbagePilesManaged = Int32.Parse(txtNoOfGarbagePilesManaged.Text);
                h.NoOfBlockedDrainDetected = Int32.Parse(txtNoOfBlockedDrainDetected.Text);
                h.NoOfBlockedDrainManaged = Int32.Parse(txtNoOfBlockedDrainManaged.Text);
                h.NoOfLowLandWithStagnantWaterDetected = Int32.Parse(txtNoOfLowLandWithStagnantWaterDetected.Text);
                h.NoOfLowLandWithStagnantWaterManaged = Int32.Parse(txtNoOfLowLandWithStagnantWaterManaged.Text);
                h.NoOfTyreRepairingRecyclingFactoryDectected = Int32.Parse(txtNoOfTyreRepairingRecyclingFactoryDectected.Text);
                h.NoOfTyreRepairingRecyclingFactoryManaged = Int32.Parse(txtNoOfTyreRepairingRecyclingFactoryManaged.Text);
                h.NoOfUnsafeConstractionSiteDetected = Int32.Parse(txtNoOfUnsafeConstractionSiteDetected.Text);
                h.NoOfUnsafeConstractionSiteManaged = Int32.Parse(txtNoOfUnsafeConstractionSiteManaged.Text);
                h.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = Int32.Parse(txtNoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae.Text);
                h.NoOfCanalDitchCesspoolManagedMosquitoLarvae = Int32.Parse(txtNoOfCanalDitchCesspoolManagedMosquitoLarvae.Text);

                PnchtId = Int32.Parse(Session["pc"].ToString());
                h.PanchayetId = PnchtId;


                db.VCTs.Add(h);
                db.SaveChanges();

                this.getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Select Sansad Name')", true);

            reset();
        }
        private void getVCTGrid(string month, int week, int day, int PnchtId)
        {
            if (month != "" || month != "Select")
            {
                if (week > 0 && day > 0)
                {
                    var s = db.VCTs.Where(l => l.month == month && l.week == week && l.day == day && l.PanchayetId == PnchtId).Select(m => new
                    {
                        id = m.vctId,
                        SansadName = m.GramSansadName,
                        SansadId = m.GramSansadId,

                        NoOfGarbagePilesDetected = m.NoOfGarbagePilesDetected,
                        NoOfGarbagePilesManaged = m.NoOfGarbagePilesManaged,
                        NoOfBlockedDrainDetected = m.NoOfBlockedDrainDetected,
                        NoOfBlockedDrainManaged = m.NoOfBlockedDrainManaged,
                        NoOfLowLandWithStagnantWaterDetected = m.NoOfLowLandWithStagnantWaterDetected,
                        NoOfLowLandWithStagnantWaterManaged = m.NoOfLowLandWithStagnantWaterManaged,
                        NoOfTyreRepairingRecyclingFactoryDectected = m.NoOfTyreRepairingRecyclingFactoryDectected,
                        NoOfTyreRepairingRecyclingFactoryManaged = m.NoOfTyreRepairingRecyclingFactoryManaged,
                        NoOfUnsafeConstractionSiteDetected = m.NoOfUnsafeConstractionSiteDetected,
                        NoOfUnsafeConstractionSiteManaged = m.NoOfUnsafeConstractionSiteManaged,
                        NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = m.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae,
                        NoOfCanalDitchCesspoolManagedMosquitoLarvae = m.NoOfCanalDitchCesspoolManagedMosquitoLarvae,
                    }).ToList();

                    gvVCT.DataSource = s;
                    gvVCT.DataBind();
                }
                else if (week > 0)
                {
                    var s = db.VCTs.Where(l => l.month == month && l.week == week && l.PanchayetId == PnchtId).Select(m => new
                    {
                        id = m.vctId,
                        SansadName = m.GramSansadName,
                        SansadId = m.GramSansadId,

                        NoOfGarbagePilesDetected = m.NoOfGarbagePilesDetected,
                        NoOfGarbagePilesManaged = m.NoOfGarbagePilesManaged,
                        NoOfBlockedDrainDetected = m.NoOfBlockedDrainDetected,
                        NoOfBlockedDrainManaged = m.NoOfBlockedDrainManaged,
                        NoOfLowLandWithStagnantWaterDetected = m.NoOfLowLandWithStagnantWaterDetected,
                        NoOfLowLandWithStagnantWaterManaged = m.NoOfLowLandWithStagnantWaterManaged,
                        NoOfTyreRepairingRecyclingFactoryDectected = m.NoOfTyreRepairingRecyclingFactoryDectected,
                        NoOfTyreRepairingRecyclingFactoryManaged = m.NoOfTyreRepairingRecyclingFactoryManaged,
                        NoOfUnsafeConstractionSiteDetected = m.NoOfUnsafeConstractionSiteDetected,
                        NoOfUnsafeConstractionSiteManaged = m.NoOfUnsafeConstractionSiteManaged,
                        NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = m.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae,
                        NoOfCanalDitchCesspoolManagedMosquitoLarvae = m.NoOfCanalDitchCesspoolManagedMosquitoLarvae,
                    }).ToList();

                    gvVCT.DataSource = s;
                    gvVCT.DataBind();
                }
                else if (day > 0)
                {
                    var s = db.VCTs.Where(l => l.month == month && l.day == day && l.PanchayetId == PnchtId).Select(m => new
                    {
                        id = m.vctId,
                        SansadName = m.GramSansadName,
                        SansadId = m.GramSansadId,
                        NoOfGarbagePilesDetected = m.NoOfGarbagePilesDetected,
                        NoOfGarbagePilesManaged = m.NoOfGarbagePilesManaged,
                        NoOfBlockedDrainDetected = m.NoOfBlockedDrainDetected,
                        NoOfBlockedDrainManaged = m.NoOfBlockedDrainManaged,
                        NoOfLowLandWithStagnantWaterDetected = m.NoOfLowLandWithStagnantWaterDetected,
                        NoOfLowLandWithStagnantWaterManaged = m.NoOfLowLandWithStagnantWaterManaged,
                        NoOfTyreRepairingRecyclingFactoryDectected = m.NoOfTyreRepairingRecyclingFactoryDectected,
                        NoOfTyreRepairingRecyclingFactoryManaged = m.NoOfTyreRepairingRecyclingFactoryManaged,
                        NoOfUnsafeConstractionSiteDetected = m.NoOfUnsafeConstractionSiteDetected,
                        NoOfUnsafeConstractionSiteManaged = m.NoOfUnsafeConstractionSiteManaged,
                        NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = m.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae,
                        NoOfCanalDitchCesspoolManagedMosquitoLarvae = m.NoOfCanalDitchCesspoolManagedMosquitoLarvae,
                    }).ToList();

                    gvVCT.DataSource = s;
                    gvVCT.DataBind();
                }
                else
                {
                    var s = db.VCTs.Where(l => l.month == month && l.PanchayetId == PnchtId).Select(m => new
                    {
                        id = m.vctId,
                        SansadName = m.GramSansadName,
                        SansadId = m.GramSansadId,
                        NoOfGarbagePilesDetected = m.NoOfGarbagePilesDetected,
                        NoOfGarbagePilesManaged = m.NoOfGarbagePilesManaged,
                        NoOfBlockedDrainDetected = m.NoOfBlockedDrainDetected,
                        NoOfBlockedDrainManaged = m.NoOfBlockedDrainManaged,
                        NoOfLowLandWithStagnantWaterDetected = m.NoOfLowLandWithStagnantWaterDetected,
                        NoOfLowLandWithStagnantWaterManaged = m.NoOfLowLandWithStagnantWaterManaged,
                        NoOfTyreRepairingRecyclingFactoryDectected = m.NoOfTyreRepairingRecyclingFactoryDectected,
                        NoOfTyreRepairingRecyclingFactoryManaged = m.NoOfTyreRepairingRecyclingFactoryManaged,
                        NoOfUnsafeConstractionSiteDetected = m.NoOfUnsafeConstractionSiteDetected,
                        NoOfUnsafeConstractionSiteManaged = m.NoOfUnsafeConstractionSiteManaged,
                        NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = m.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae,
                        NoOfCanalDitchCesspoolManagedMosquitoLarvae = m.NoOfCanalDitchCesspoolManagedMosquitoLarvae,
                    }).ToList();

                    gvVCT.DataSource = s;
                    gvVCT.DataBind();
                }
            }
            else
            {
                var s = db.VCTs.Where(l => l.PanchayetId == PnchtId).Select(m => new
                {
                    id = m.vctId,
                    SansadName = m.GramSansadName,
                    SansadId = m.GramSansadId,
                    NoOfGarbagePilesDetected = m.NoOfGarbagePilesDetected,
                    NoOfGarbagePilesManaged = m.NoOfGarbagePilesManaged,
                    NoOfBlockedDrainDetected = m.NoOfBlockedDrainDetected,
                    NoOfBlockedDrainManaged = m.NoOfBlockedDrainManaged,
                    NoOfLowLandWithStagnantWaterDetected = m.NoOfLowLandWithStagnantWaterDetected,
                    NoOfLowLandWithStagnantWaterManaged = m.NoOfLowLandWithStagnantWaterManaged,
                    NoOfTyreRepairingRecyclingFactoryDectected = m.NoOfTyreRepairingRecyclingFactoryDectected,
                    NoOfTyreRepairingRecyclingFactoryManaged = m.NoOfTyreRepairingRecyclingFactoryManaged,
                    NoOfUnsafeConstractionSiteDetected = m.NoOfUnsafeConstractionSiteDetected,
                    NoOfUnsafeConstractionSiteManaged = m.NoOfUnsafeConstractionSiteManaged,
                    NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = m.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae,
                    NoOfCanalDitchCesspoolManagedMosquitoLarvae = m.NoOfCanalDitchCesspoolManagedMosquitoLarvae,
                }).ToList();

                gvVCT.DataSource = s;
                gvVCT.DataBind();
            }

        }
        private void reset()
        {
            //ddlSansadName.SelectedIndex = -1;
            //ddlDay.SelectedIndex = -1;
            //ddlWeek.SelectedIndex = -1;
            //txtBlockId.Text = "";
            //txtDistrictId.Text = "";
            //txtGramPanchayatId.Text = "";
            //txtGramSansadId.Text = "";

            txtNoOfGarbagePilesDetected.Text = "";
            txtNoOfGarbagePilesManaged.Text = "";
            txtNoOfBlockedDrainDetected.Text = "";
            txtNoOfBlockedDrainManaged.Text = "";
            txtNoOfLowLandWithStagnantWaterDetected.Text = "";
            txtNoOfLowLandWithStagnantWaterManaged.Text = "";
            txtNoOfTyreRepairingRecyclingFactoryDectected.Text = "";
            txtNoOfTyreRepairingRecyclingFactoryManaged.Text = "";
            txtNoOfUnsafeConstractionSiteDetected.Text = "";
            txtNoOfUnsafeConstractionSiteManaged.Text = "";
            txtNoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae.Text = "";
            txtNoOfCanalDitchCesspoolManagedMosquitoLarvae.Text = "";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }

        protected void ddlSansadName_SelectedIndexChanged(object sender, EventArgs e)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            PnchtId = Int32.Parse(Session["pc"].ToString());
            SansadMaster s = db.SansadMasters.Single(l => l.GSId==gsId && l.PanchayetId == PnchtId);

            txtBlockId.Text = s.BlockId.ToString();
            txtDistrictId.Text = s.DistrictId.ToString();
            txtGramPanchayatId.Text = s.GramPanchayatId.ToString();
            txtGramSansadId.Text = s.GramSansadId.ToString();
        }

        protected void gvVCT_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvVCT.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvVCT_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvVCT.EditIndex = e.NewEditIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
        }

        protected void gvVCT_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvVCT.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
        }

        protected void gvVCT_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvVCT.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvVCT.DataKeys[e.RowIndex].Values[0]);
            string SansadName = (row.Cells[2].Controls[0] as TextBox).Text;
            string SansadId = (row.Cells[3].Controls[0] as TextBox).Text;

            string NoOfGarbagePilesDetected = (row.Cells[4].Controls[0] as TextBox).Text;
            string NoOfGarbagePilesManaged = (row.Cells[5].Controls[0] as TextBox).Text;
            string NoOfBlockedDrainDetected = (row.Cells[6].Controls[0] as TextBox).Text;
            string NoOfBlockedDrainManaged = (row.Cells[7].Controls[0] as TextBox).Text;
            string NoOfLowLandWithStagnantWaterDetected = (row.Cells[8].Controls[0] as TextBox).Text;
            string NoOfLowLandWithStagnantWaterManaged = (row.Cells[9].Controls[0] as TextBox).Text;
            string NoOfTyreRepairingRecyclingFactoryDectected = (row.Cells[10].Controls[0] as TextBox).Text;
            string NoOfTyreRepairingRecyclingFactoryManaged = (row.Cells[11].Controls[0] as TextBox).Text;
            string NoOfUnsafeConstractionSiteDetected = (row.Cells[12].Controls[0] as TextBox).Text;
            string NoOfUnsafeConstractionSiteManaged = (row.Cells[13].Controls[0] as TextBox).Text;
            string NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = (row.Cells[14].Controls[0] as TextBox).Text;
            string NoOfCanalDitchCesspoolManagedMosquitoLarvae = (row.Cells[15].Controls[0] as TextBox).Text;



            VCT m = db.VCTs.Single(s => s.vctId == id);

            m.GramSansadName = SansadName;
            m.GramSansadId = Int32.Parse(SansadId);

            m.NoOfGarbagePilesDetected = Int32.Parse(NoOfGarbagePilesDetected);
            m.NoOfGarbagePilesManaged = Int32.Parse(NoOfGarbagePilesManaged);
            m.NoOfBlockedDrainDetected = Int32.Parse(NoOfBlockedDrainDetected);
            m.NoOfBlockedDrainManaged = Int32.Parse(NoOfBlockedDrainManaged);
            m.NoOfLowLandWithStagnantWaterDetected = Int32.Parse(NoOfLowLandWithStagnantWaterDetected);
            m.NoOfLowLandWithStagnantWaterManaged = Int32.Parse(NoOfLowLandWithStagnantWaterManaged);
            m.NoOfTyreRepairingRecyclingFactoryDectected = Int32.Parse(NoOfTyreRepairingRecyclingFactoryDectected);
            m.NoOfTyreRepairingRecyclingFactoryManaged = Int32.Parse(NoOfTyreRepairingRecyclingFactoryManaged);
            m.NoOfUnsafeConstractionSiteDetected = Int32.Parse(NoOfUnsafeConstractionSiteDetected);
            m.NoOfUnsafeConstractionSiteManaged = Int32.Parse(NoOfUnsafeConstractionSiteManaged);
            m.NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae = Int32.Parse(NoOfCanalDitchCesspoolFoundToHaveMosquitoLarvae);
            m.NoOfCanalDitchCesspoolManagedMosquitoLarvae = Int32.Parse(NoOfCanalDitchCesspoolManagedMosquitoLarvae);



            db.SaveChanges();


            gvVCT.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
        }

        protected void gvVCT_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvVCT.DataKeys[e.RowIndex].Values[0]);
            db.VCTs.Where(s => s.vctId == id).Delete();

            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
        }

        protected void gvVCT_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVCT.PageIndex = e.NewPageIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            this.getVCTGrid(ddlSearchMonth.Items[1].Value, 0, 0, PnchtId);
        }

        protected void ddlSearchMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getVCTGrid(ddlSearchMonth.SelectedValue, ddlSearchWeek.SelectedIndex, ddlSrearchDay.SelectedIndex, PnchtId);
        }

        protected void ddlSearchWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getVCTGrid(ddlSearchMonth.SelectedValue, ddlSearchWeek.SelectedIndex, ddlSrearchDay.SelectedIndex, PnchtId);
        }

        protected void ddlSrearchDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getVCTGrid(ddlSearchMonth.SelectedValue, ddlSearchWeek.SelectedIndex, ddlSrearchDay.SelectedIndex, PnchtId);
        }
    }
}