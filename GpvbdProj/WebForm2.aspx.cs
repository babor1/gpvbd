﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GpvbdProj
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
            }
        }

        protected void btnTest_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "Success....";
        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            txtName.Text = "Nadeem Qusroo";

        }
    }
}