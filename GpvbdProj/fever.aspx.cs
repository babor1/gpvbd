﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public partial class fever : System.Web.UI.Page
    {
        gpvbdDBEntities db = new gpvbdDBEntities(); 
        int PnchtId = 0;
        bool slv = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PnchtId = Int32.Parse(Session["pc"].ToString());
                getFeverGridQrStr(PnchtId, slv);
                getSansadDDL(PnchtId);
            }
        }
        private void getSansadDDL(int PnchtId)
        {
            var c = db.SansadMasters.Where(l => l.PanchayetId == PnchtId).Select(m => new { id = m.GSId, Name = m.GramSansadName }).ToList();

            ddlSansadName.DataSource = c;
            ddlSansadName.DataValueField = "id";
            ddlSansadName.DataTextField = "name";
            ddlSansadName.DataBind();

            ddlSansadName.Items.Insert(0, new ListItem("Select", "0"));

            ddlSansadName.SelectedIndex = -1;
        }
        private void getHouseNo(int PnchtId)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            var c = db.H2HMaster.Where(s => s.GSId == gsId && s.PanchayetId == PnchtId).Select(m => new { houseName = m.HouseNo }).ToList();

            ddlHouseNo.DataSource = c;
            ddlHouseNo.DataValueField = "houseName";
            ddlHouseNo.DataTextField = "houseName";
            ddlHouseNo.DataBind();

            ddlHouseNo.Items.Insert(0, new ListItem("Select", "0"));

            ddlHouseNo.SelectedIndex = -1;

        }
        private void getRecordSelectedHouse(int PnchtId)
        {
            int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
            string HouseNo = ddlHouseNo.SelectedItem.Text;
            var c = db.H2HMaster.Where(s =>s.GSId==gsId && s.HouseNo == HouseNo && s.PanchayetId == PnchtId).FirstOrDefault();

            txtName.Text = c.Name;
            if (c.FatherName == null)
                txtFatherName.Text = "";
            else
                txtFatherName.Text = c.FatherName;
            if (c.MobileNo == null)
                txtMobileNo.Text = "";
            else
                txtMobileNo.Text = c.MobileNo;

        }


        private void getFeverGridQrStr(int PnchtId, bool slv)
        {
            if (Request.QueryString["id"] == null)
            {
                var s = db.FeverMasters.Where(l => l.PanchayetId == PnchtId && l.isSolved == slv).Select(m => new
                {
                    id = m.fmId,
                    HouseNo = m.HouseNo,
                    MobileNo = m.MobileNo,
                    SansadName = m.SansadName,
                    FatherName = m.FatherName,
                    Name = m.Name,
                    FeverPatientName = m.FeverPatientName,
                    FeverFatherPatientName = m.FeverFatherPatientName,
                    DayofFever = m.DayofFever,
                    Area = m.Area,
                    Status = m.isSolved,
                    SolvedDate = m.SolvedDate,
                    EntryDate = m.EntryDate
                }).ToList();

                gvFever.DataSource = s;
                gvFever.DataBind();
            }
            else
            {
                int h2hid = Int32.Parse(Request.QueryString["id"].ToString());
                var s = db.FeverMasters.Where(l => l.H2Hid == h2hid && l.PanchayetId == PnchtId && l.isSolved == slv).Select(m => new
                {
                    id = m.fmId,
                    HouseNo = m.HouseNo,
                    MobileNo = m.MobileNo,
                    SansadName = m.SansadName,
                    FatherName = m.FatherName,
                    Name = m.Name,
                    FeverPatientName = m.FeverPatientName,
                    FeverFatherPatientName = m.FeverFatherPatientName,
                    DayofFever = m.DayofFever,
                    Area = m.Area,
                    Status = m.isSolved,
                    SolvedDate = m.SolvedDate,
                    EntryDate =m.EntryDate
                }).ToList();

                gvFever.DataSource = s;
                gvFever.DataBind();
            }


        }

        protected void gvFever_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvFever.EditIndex)
            {
                (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            }
        }

        protected void gvFever_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvFever.EditIndex = e.NewEditIndex;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            if (ddlsearchStatus.SelectedIndex == 0)
                slv = false;
            else
                slv = true;
            this.getFeverGridQrStr(PnchtId, slv);
        }

        protected void gvFever_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvFever.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            if (ddlsearchStatus.SelectedIndex == 0)
                slv = false;
            else
                slv = true;
            this.getFeverGridQrStr(PnchtId, slv);
        }

        protected void gvFever_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gvFever.Rows[e.RowIndex];
            int id = Convert.ToInt32(gvFever.DataKeys[e.RowIndex].Values[0]);
            string SansadName = (row.Cells[2].Controls[0] as TextBox).Text;
            string HouseNo = (row.Cells[3].Controls[0] as TextBox).Text;
            string FeverPatientName = (row.Cells[4].Controls[0] as TextBox).Text;
            string FeverFatherPatientName = (row.Cells[5].Controls[0] as TextBox).Text;
            string Name = (row.Cells[6].Controls[0] as TextBox).Text;
            string FatherName = (row.Cells[7].Controls[0] as TextBox).Text;
            string DayofFever = (row.Cells[8].Controls[0] as TextBox).Text;
            string Area = (row.Cells[9].Controls[0] as TextBox).Text;
            string MobileNo = (row.Cells[10].Controls[0] as TextBox).Text;
            string EntryDate = (row.Cells[11].Controls[0] as TextBox).Text;
            string IsSolved = (row.Cells[12].Controls[0] as TextBox).Text;
            string SolvedDate = (row.Cells[13].Controls[0] as TextBox).Text;



            FeverMaster m = db.FeverMasters.Single(s => s.fmId == id);

            m.SansadName = SansadName;
            m.HouseNo = HouseNo;
            m.Name = Name.ToUpper();
            m.FatherName = FatherName.ToUpper();
            m.FeverPatientName = FeverPatientName.ToUpper();
            m.FeverFatherPatientName = FeverFatherPatientName.ToUpper();
            m.DayofFever = DayofFever;
            m.Area = Area.ToUpper();
            m.MobileNo = MobileNo;

            db.SaveChanges();


            gvFever.EditIndex = -1;
            PnchtId = Int32.Parse(Session["pc"].ToString());
            if (ddlsearchStatus.SelectedIndex == 0)
                slv = false;
            else
                slv = true;
            this.getFeverGridQrStr(PnchtId, slv);
        }

        protected void gvFever_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(gvFever.DataKeys[e.RowIndex].Values[0]);
            db.FeverMasters.Where(s => s.fmId == id).Delete();
            PnchtId = Int32.Parse(Session["pc"].ToString());
            if (ddlsearchStatus.SelectedIndex == 0)
                slv = false;
            else
                slv = true;
            this.getFeverGridQrStr(PnchtId, slv);
        }
        private bool checkEmptyEntry()
        {
            if (txtName.Text == "" || txtFatherName.Text == "" || txtFevPtName.Text == "" || txtFevPtFatherName.Text == "" || txtDayOfFever.Text == "" || txtArea.Text == "")
                return true;
            else
                return false;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if(!checkEmptyEntry())
                {
                    PnchtId = Int32.Parse(Session["pc"].ToString());
                    int gsId = Int32.Parse(ddlSansadName.SelectedItem.Value);
                    if (ddlSansadName.SelectedItem.Text != "Select")
                    {
                        FeverMaster h = new FeverMaster();
                        h.SansadName = ddlSansadName.SelectedItem.Text;
                        h.GSId = gsId;
                        h.HouseNo = ddlHouseNo.Text;
                        h.FeverPatientName = txtFevPtName.Text.ToUpper();
                        h.FeverFatherPatientName = txtFevPtFatherName.Text.ToUpper();
                        h.Name = txtName.Text.ToUpper();
                        h.FatherName = txtFatherName.Text.ToUpper();
                        h.DayofFever = txtDayOfFever.Text;
                        h.Area = txtArea.Text.ToUpper();
                        h.MobileNo = txtMobileNo.Text;
                        h.EntryDate = DateTime.Now;
                        h.PanchayetId = Int32.Parse(Session["pc"].ToString());
                        h.isSolved = false;

                        db.FeverMasters.Add(h);
                        db.SaveChanges();

                        //int cnt = Int32.Parse(labDisp.Text) + 1;
                        //labDisp.Text = cnt.ToString();


                        this.getFeverGridQrStr(PnchtId, slv);

                        Response.Redirect("/fever.aspx");
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Select Sansad Name')", true);
                }
                

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + ex.Message + "')", true);
            }

            reset();
        }
        private void reset()
        {
            //ddlSansadName.SelectedIndex = -1;
            //ddlHouseNo.SelectedIndex = -1;
            //txtName.Text = "";
            // txtFatherName.Text = "";
            txtArea.Text = "";
            txtFevPtFatherName.Text = "";
            txtFevPtName.Text = "";
            //txtMobileNo.Text = "";
        }

        protected void ddlSansadName_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getHouseNo(PnchtId);
        }

        protected void ddlHouseNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            getRecordSelectedHouse(PnchtId);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }

        protected void ddlsearchStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            PnchtId = Int32.Parse(Session["pc"].ToString());
            if (ddlsearchStatus.SelectedIndex == 0)
                slv = false;
            else
                slv = true;
            this.getFeverGridQrStr(PnchtId, slv);
        }

      

        protected void btnGetReoprt_Click(object sender, EventArgs e)
        {
            bool isSolved = false;
            if (ddlsearchStatus.SelectedIndex == 0)
                isSolved = false;
            else
                isSolved = true;

            Response.Redirect("/FeverReport.aspx?is=" + isSolved);
        }
    }
}