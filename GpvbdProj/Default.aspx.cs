﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GpvbdProj
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnComplain_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Complain.aspx");
        }

        protected void btnUserLogIn_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Login.aspx");
        }
    }
}