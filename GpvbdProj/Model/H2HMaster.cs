//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GpvbdProj.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class H2HMaster
    {
        public int h2hMid { get; set; }
        public Nullable<int> GSId { get; set; }
        public string SansadName { get; set; }
        public string HouseNo { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public Nullable<int> FamilyMember { get; set; }
        public string MobileNo { get; set; }
        public Nullable<int> PanchayetId { get; set; }
    
        public virtual PanchayetMster PanchayetMster { get; set; }
        public virtual SansadMaster SansadMaster { get; set; }
    }
}
