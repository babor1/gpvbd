﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="GpvbdProj.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
       <div>
            <div>
        <asp:ScriptManager runat="server" ID="Scr">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="upl">
            <contenttemplate>
         <div id="main-content">
                 <ul class="nav nav-pills" id="myTab" role="tablist">
                     <li class="nav-item">
                         <a class="nav-link active" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-selected="true">Asset Details</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" id="employee-tab" data-toggle="tab" href="#employee" role="tab" aria-controls="employee" aria-selected="false">Employee Information</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Related Documents</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" id="transfer-tab" data-toggle="tab" href="#transfer" role="tab" aria-controls="transfer" aria-selected="false">Transfer History</a>
                     </li>
                 </ul>
                 <div class="tab-content" id="myTabContent">
                     <div class="tab-pane fade show active" id="information" role="tabpanel" aria-labelledby="information-tab">
                         Tab-1 content
                        
                     </div>
                     <div class="tab-pane fade" id="employee" role="tabpanel" aria-labelledby="employee-tab">
                         <h4 class="mb-3">Employee Details</h4>
                         <div class="row">
   
                             <div class="col-md-3 mb-3">
                                 <label for="txtEmpId">Employee Id<span class="text-muted">(Required)</label>
                                 <asp:RequiredFieldValidator runat="server" ID="rfv7" ControlToValidate="txtEmpId" ErrorMessage="*" ForeColor="Red" Display="Dynamic" Font-Bold="true" SetFocusOnError="true">
                                 </asp:RequiredFieldValidator>
                                 <asp:TextBox runat="server" ID="txtEmpId" CssClass="form-control" AutoPostBack="true"  MaxLength="6"></asp:TextBox>
   
                             </div>
                             <div class="col-md-6 mb-3">
                                 <label for="txtName">Name<span class="text-muted">&nbsp;(Full name & other details will be automatically fetched based on EmpId)</span></label>
                                 <asp:TextBox runat="server" ID="txtName" CssClass="form-control" ReadOnly="true" OnTextChanged="txtName_TextChanged"></asp:TextBox>
                             </div>
                         </div>
                         <div class="row">
                             <div class="col-md-4 mb-3">
                                 <label for="ddlDepartment">Company Code</label>
                                 <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="ddlCompany" InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic" Font-Bold="true" SetFocusOnError="true">
                                 </asp:RequiredFieldValidator>
                                 <asp:DropDownList runat="server" ID="ddlCompany" CssClass="form-control" AppendDataBoundItems="true">
                                     <asp:ListItem Text="Please select" Value="0" Selected="True"></asp:ListItem>
                                 </asp:DropDownList>
                             </div>
                             <div class="col-md-4 mb-3">
                                 <label for="ddlDepartment">Department</label>
                                 <asp:RequiredFieldValidator runat="server" ID="rfv8" InitialValue="0" ControlToValidate="ddlDepartment" ErrorMessage="*" ForeColor="Red" Display="Dynamic" Font-Bold="true" SetFocusOnError="true">
                                 </asp:RequiredFieldValidator>
                                 <asp:DropDownList runat="server" ID="ddlDepartment" AppendDataBoundItems="true" CssClass="form-control">
                                     <asp:ListItem Text="Please Select" Value="0"></asp:ListItem>
                                 </asp:DropDownList>
                             </div>
                             <div class="col-md-4 mb-3">
                                 <label for="ddlLocation">Location</label>
                                 <asp:RequiredFieldValidator runat="server" ID="rfv9" InitialValue="0" ControlToValidate="ddlLocation" ErrorMessage="*" ForeColor="Red" Display="Dynamic" Font-Bold="true" SetFocusOnError="true">
                                 </asp:RequiredFieldValidator>
                                 <asp:DropDownList runat="server" ID="ddlLocation" AppendDataBoundItems="true" CssClass="form-control">
                                     <asp:ListItem Text="Please Select" Value="0"></asp:ListItem>
                                 </asp:DropDownList>
   
                             </div>
   
                         </div>
                     </div>
                     <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                         Tab Content-3
                     </div>
                     <div class="tab-pane fade" id="transfer" role="tabpanel" aria-labelledby="transfer-tab">
                         <asp:Label runat="server" ID="lblMsg"></asp:Label>
                       <asp:Button runat="server" ID="btnTest" Text="Test" OnClick="btnTest_Click" CssClass="btn btn-primary" CausesValidation="false" />
                     </div>
                 </div>
             </div>
             <asp:HiddenField ID="TabName" runat="server" /> 
         </contenttemplate>
        </asp:UpdatePanel>
        <script type="text/javascript">
            $(function () {
                SetTabs();
            });
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        SetTabs();
                    }
                });
            };
            function SetTabs() {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "information";
                $('#main-content a[href="#' + tabName + '"]').tab('show');
                $("#main-content a").click(function () {
                    $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
                });
            };
        </script>
    </div>
        </div>
</asp:Content>
