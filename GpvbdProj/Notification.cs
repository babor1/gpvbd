﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GpvbdProj.Model;

namespace GpvbdProj
{
    public class Notification
    {
        gpvbdDBEntities db = new gpvbdDBEntities();
        public string getFeverNotf(int PnchId)
        {

            int f = db.FeverMasters.Where(l => l.PanchayetId == PnchId && l.isSolved == false).Count();

            return f.ToString();
        }

        public string getComplaintNotf(int PnchId)
        {

            int f = db.PublicComplains.Where(l => l.PanchayetId == PnchId && l.isSolved == false).Count();

            return f.ToString();
        }
    }


}